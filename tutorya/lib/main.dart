import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tutorya/blocs/user/bloc/user_bloc.dart';
import 'package:tutorya/singletons/singletonConnectivity.dart';
import 'package:tutorya/singletons/singletonDataBase.dart';
import 'package:tutorya/viewModels/globalUser_view_model.dart';
import 'package:tutorya/viewModels/mentorship_list_view_model.dart';
import 'package:tutorya/viewModels/movie_list_view_model.dart';
import 'package:tutorya/viewModels/tutor_list_view_model.dart';
import 'package:tutorya/views/Home/home.dart';
import 'package:tutorya/views/SignIn/logIn.dart';
import 'package:tutorya/views/SignIn/signIn.dart';
import 'package:tutorya/views/authenticate/authenticate.dart';
import 'package:tutorya/views/profileScreen/profile_view.dart';
import 'package:tutorya/views/settings/settings.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Initialize FFI
  NetworkConnectivity.instance.initialise();
  LocalDatabase.instance.database;
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  FirebaseFirestore.instance.settings = Settings(persistenceEnabled: false);
  FlutterError.onError = (errorDetails) {
    FirebaseCrashlytics.instance.recordFlutterFatalError(errorDetails);
  };
  // Pass all uncaught asynchronous errors that aren't handled by the Flutter framework to Crashlytics
  PlatformDispatcher.instance.onError = (error, stack) {
    FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);
    return true;
  };
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => MentorshipListViewModel(),
        ),
        ChangeNotifierProvider(
          create: (context) => GlobalUserViewModel(),
        ),
        ChangeNotifierProvider(
          create: (context) => TutorListViewModel(),
        ),
        BlocProvider(create: (context) => UserBloc())
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData.dark(),
        initialRoute: '/',
        routes: {
          '/': (context) => LogInScreen(),
          '/home': (context) => HomeScreen(),
          '/register': (context) => RegistrationScreen(),
          '/settings':(context) => SettingsScreen()
        },
      ),
    );
  }
}
