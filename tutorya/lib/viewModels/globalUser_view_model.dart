import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:tutorya/models/mentorship.dart';
import 'package:tutorya/models/userApp.dart';
import 'package:tutorya/services/userservice.dart';
import 'package:tutorya/viewModels/mentorship_view_model.dart';

class GlobalUserViewModel extends ChangeNotifier {
  String id;
  String career;
  String name;
  String photo;
  int type;
  List<Mentorship> mentorships = [];
  List<MentorshipViewModel> mentorshipsVM = [];
  bool isLoading=false;
  int cantClicks=0;
  late DateTime startTime;

  GlobalUserViewModel({
    this.id = "",
    this.career = "",
    this.name = "",
    this.photo = "",
    this.type = 0,
    this.cantClicks=0,
  });

  Future<UserApp> get getGlobUser {
    final us = UserService().getUserById(this.id);
    return us;
  }

  Future<void> setUser(
      String setId, String setCareer, String name, String photo, int pType, List<Mentorship> pMents, DateTime startTime) async {
    this.id = setId;
    this.career = setCareer;
    this.name = name;
    this.photo = photo;
    this.type = pType;
    this.mentorships = pMents;
    this.startTime = startTime;
  }

  Future<void> setPhoto(String photo) async {
    this.photo = photo;
    notifyListeners();
  }

  Future<void> aumentarContadorClicks() async{
    cantClicks=cantClicks+1;
  }

  Future<void> insertAndUpdateClick() async{
    print("Cant Clicks $cantClicks");
    await aumentarContadorClicks();
    await UserService().insertClick(cantClicks);
    print("Sirvio");
    await UserService().calculateAverageClicks(id);
    print("Sirvio final");
    cantClicks=0;
  }

  Future<void> addMentorshipToUser(Mentorship mentorship) async {
    mentorships.add(mentorship);
    UserService().addMentorshipToUser(this.id, mentorship);
    UserService().saveMentorshipToLocalFile(mentorship);
  }

  Future<void> obtainMentorships(bool online) async {
    isLoading=true;
    mentorships=[];
    if(online==true){
      mentorships=await UserService().getUserMentorshipsId(id);
    }
    else {
      mentorships=await UserService().loadMentorshipsFromLocalFile();
    }
    mentorshipsVM=mentorships.map((item) => MentorshipViewModel(mentorship: item)).toList();
    isLoading=false;
    notifyListeners();
  }

  bool alreadyEnrolled(String mentorship_id)  {
    // Verificar si el usuario tiene mentorías
    if (mentorships.isNotEmpty) {
      // Recorrer la lista de mentorías del usuario
      for (final Mentorship mentorship in mentorships) {
        if (mentorship.id == mentorship_id) {
          // La mentoría con el ID dado ya existe en el listado
          return true;
        }
      }
    }
    // La mentoría con el ID dado no existe en el listado
    return false;

  }

}
