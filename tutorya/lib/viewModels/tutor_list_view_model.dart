import 'package:flutter/material.dart';
import 'package:tutorya/services/userservice.dart';
import '../services/userservice.dart';
import 'package:tutorya/viewModels/tutor_view_model.dart';

class TutorListViewModel extends ChangeNotifier {

  List<TutorViewModel> tutors = <TutorViewModel>[];

  Future<void> fetchTutors() async {
    final results = await UserService().getAllTutors();
    tutors = results.map((item) => TutorViewModel(tutor: item)).toList();
    notifyListeners();
  }
}
