import 'package:tutorya/models/mentorship.dart';
import 'package:tutorya/models/movie.dart';
import 'package:tutorya/models/userApp.dart';

class TutorViewModel {

  final UserApp tutor;
  int cont;

  TutorViewModel({
    required this.tutor,
    this.cont=0
  });

  List<Mentorship> get mentorships {
    return this.tutor.mentorships;
  }

  UserApp get getTutor {
    return this.tutor;
  }

  void get contUp {
    cont=1+cont;
  }

  int get getCont {
    return cont;
  }

  String get id {
    return this.tutor.id;
  }

  String get getPhoto {
    return this.tutor.photo;
  }

  String get getName {
    return this.tutor.name;
  }

  int get getRate {
    return this.tutor.rate;
  }

}
