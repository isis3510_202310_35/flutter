import 'package:tutorya/models/course.dart';
import 'package:tutorya/models/mentorship.dart';
import 'package:tutorya/models/movie.dart';
import 'package:tutorya/models/userApp.dart';

class CourseViewModel {

  final Course course;

  CourseViewModel({
    required this.course,
  });

  String get getname {
    return course.name;
  }

  int get getCont {
    return course.count;
  }

}