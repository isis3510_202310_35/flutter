import 'package:flutter/material.dart';
import 'package:tutorya/services/courseservice.dart';
import 'package:tutorya/services/mentorshipservice.dart';
import 'package:tutorya/services/userservice.dart';
import 'package:tutorya/viewModels/course_view_model.dart';
import 'package:tutorya/viewModels/mentorship_view_model.dart';
import 'package:tutorya/viewModels/tutor_view_model.dart';

import '../models/mentorship.dart';

class MentorshipListViewModel extends ChangeNotifier {

  List<MentorshipViewModel> mentorships = <MentorshipViewModel>[];
  List<MentorshipViewModel> mentorshipsShared = <MentorshipViewModel>[];
  List<MentorshipViewModel> studentMentorships = <MentorshipViewModel>[];
  List<MentorshipViewModel> studentMentorshipsIdSearch = <MentorshipViewModel>[];
  List<CourseViewModel> top3Courses = <CourseViewModel>[];
  List<TutorViewModel> tutorsVm = <TutorViewModel>[];
  bool isLoading=false;
  // Shared
  String lastSearch="";

  Future<void> fetchMentorships(String keyword) async {
    isLoading=true;
    final results =  await MentorshipService().getAllMentorshipsByCourse(keyword);
    mentorships = results.map((item) => MentorshipViewModel(mentorship: item)).toList();
    isLoading=false;
    notifyListeners();
  }

  Future<void> fetchMentorshipsbyCourse(String keyword) async {
    isLoading=true;
    final results =  await MentorshipService().getAllMentorshipsByCourse(keyword);
    studentMentorshipsIdSearch = results.map((item) => MentorshipViewModel(mentorship: item)).toList();
    isLoading=false;
    notifyListeners();
  }

  Future<void> fetchMentorshipsShared() async {
    final results =  await MentorshipService().getAllMentorshipsShared();
    mentorshipsShared = results.map((item) => MentorshipViewModel(mentorship: item)).toList();
    notifyListeners();
  }

  Future<void> clearMentorshipsShared() async {
    lastSearch="";
    isLoading=true;
    mentorshipsShared=[];
    MentorshipService().clearSharedPreferences();
    isLoading=false;
    notifyListeners();
  }

  Future<void> fetchMentorshipsByObjectIds(List ids) async {
    isLoading=true;
    studentMentorshipsIdSearch=[];
    List<MentorshipViewModel> studentMentorshipsInside = <MentorshipViewModel>[];
    for(var id in ids){
      var result =  await MentorshipService().getMentorshipById(id);
      var mentorshipVM=MentorshipViewModel(mentorship: result);
      studentMentorshipsInside.add(mentorshipVM);
    }
    studentMentorshipsIdSearch=studentMentorshipsInside;
    isLoading=false;
    notifyListeners();
  }

  Future<void> clearMentorships() async {
    // isLoading=true;
    // this.studentMentorships=[];
    studentMentorshipsIdSearch=[];
    tutorsVm=[];
    mentorships=[];
    notifyListeners();
  }

  // Smart Features
  Future<void> fetchMentorshipsOfTop5Tutor(String keyword) async {
    if(studentMentorships.isNotEmpty){

    }
    else {
      isLoading=true;
      List<MentorshipViewModel> studentMentorshipsSmart = <MentorshipViewModel>[];
      final results =  await UserService().getStudentsByCareerWithMentorship(keyword);
      studentMentorships = []; // limpiar la lista mentorships

      // agregar todas las mentorías de los estudiantes a la lista mentorships
      for (final student in results) {
        for (final mentorship in student.mentorships) {
          var mentorshipVM=MentorshipViewModel(mentorship: mentorship);
          studentMentorshipsSmart.add(mentorshipVM);
        }
      }
      final tutors =  await UserService().getAllTutors();
      tutorsVm = [];
      for (final tutor in tutors) {
        var tutorVm=TutorViewModel(tutor: tutor);
        for (final mentorship in studentMentorshipsSmart) {
          if(tutorVm.id==mentorship.tutorid){
            tutorVm.contUp;
          }
        }
        if(tutorVm.getCont>0){
          tutorsVm.add(tutorVm);
        }
      }
      // Mayor a menor
      tutorsVm.sort((a, b) => b.getCont.compareTo(a.getCont));
      if(tutorsVm.length>3) {
        tutorsVm.take(3);
      }
      studentMentorshipsSmart = [];
      for(final tutor in tutorsVm){
        var cant=0;
        for (final mentorship in tutor.mentorships) {
          if(cant<2){
            Mentorship ment=await MentorshipService().getMentorshipById(mentorship.id);
            var mentorshipVM=MentorshipViewModel(mentorship: ment);
            studentMentorshipsSmart.add(mentorshipVM);
          }
          cant=cant+1;
        }
      }
      studentMentorships = studentMentorshipsSmart;
    }
    isLoading=false;
    notifyListeners();
  }

  Future<void> fetchMentorshipsNow() async {
    final results = await MentorshipService().getAllMentorshipsStartingSoon();
    mentorships = results.map((item) => MentorshipViewModel(mentorship: item)).toList();
    notifyListeners();
  }

  Future<void> fetchUpcomingMentorshipsFor(String name) async {
    final results = await MentorshipService().getUpcomingMentorshipsFor(name);
    mentorships = results.map((item) => MentorshipViewModel(mentorship: item)).toList();
    notifyListeners();
  }

  Future<void> getTop3Courses() async {
    final results =  await CourseService().getTopCourses();
    top3Courses = results.map((item) => CourseViewModel(course: item)).toList();
    notifyListeners();
  }

  Future<void> addCountCourse(String name) async {
    await CourseService().updateCourseCount(name);
  }

  Future<void> addUserToMentorship(String userId,Mentorship mentorship) async {
    MentorshipService().addUserToMentorship(userId, mentorship);
  }

}
