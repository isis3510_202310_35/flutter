import 'package:tutorya/models/mentorship.dart';
import 'package:tutorya/models/movie.dart';
import 'package:tutorya/models/userApp.dart';
import 'package:tutorya/services/userservice.dart';

class MentorshipViewModel {

  final Mentorship mentorship;
  int actualCapacity= 0;

  MentorshipViewModel({required this.mentorship});

  String get name {
    return this.mentorship.name;
  }

  String get photo {
    return this.mentorship.tutor.photo;
  }

  String get tutorName {
    return this.mentorship.tutor.name;
  }

  String get description {
    return this.mentorship.description;
  }

  String get tutorid {
    return this.mentorship.tutor.id;
  }

  String get id {
    return this.mentorship.id;
  }

  int get getActualCapacity {
    if(actualCapacity==0){
      actualCapacity=this.mentorship.students.length;
    }
    return actualCapacity;
  }

  void sumActualCapacity(){
  actualCapacity=actualCapacity+1;
}

  Future<void> addUser(String userid) async {
    final us = await UserService().getUserById(userid);
    this.mentorship.students.add(us);
  }

  DateTime get date {
    return DateTime.parse(this.mentorship.date);
  }

}
