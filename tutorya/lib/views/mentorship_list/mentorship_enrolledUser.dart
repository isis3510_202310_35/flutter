import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:tutorya/models/mentorship.dart';
import 'package:tutorya/singletons/singletonConnectivity.dart';
import 'package:tutorya/singletons/singletonLightSensor.dart';
import 'package:tutorya/viewModels/globalUser_view_model.dart';
import 'package:tutorya/viewModels/mentorship_list_view_model.dart';
import 'package:tutorya/viewModels/mentorship_view_model.dart';
import 'package:tutorya/widgets/mentorship_list.dart';
import 'package:provider/provider.dart';

class MentorshipsEnrolledUser extends StatefulWidget {
  final bool online;
  const MentorshipsEnrolledUser(
      {Key? key,
        required this.online,})
      : super(key: key);
  @override
  _MentorshipsEnrolledUserState createState() => _MentorshipsEnrolledUserState();
}

class _MentorshipsEnrolledUserState extends State<MentorshipsEnrolledUser> {
  List<MentorshipViewModel> mentorships = [];

  // Connection
  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;
  String string = '';

  // DarkMode
  final LightSensor _lightSensor= LightSensor.instance;
  bool darkMode=false;


  @override
  void  initState() {
    super.initState();
    _networkConnectivity.initialise();
    Provider.of<GlobalUserViewModel>(context, listen: false).aumentarContadorClicks();
    Provider.of<GlobalUserViewModel>(context, listen: false).obtainMentorships(widget.online);
  }

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<MentorshipListViewModel>(context);
    final uvm = Provider.of<GlobalUserViewModel>(context);
    return Scaffold(
      backgroundColor: _lightSensor.isActivatedDark()==true ? Colors.black12:Color(0xFFD1EDBF),
      appBar: AppBar(
          iconTheme: IconThemeData(
            color: _lightSensor.isActivatedDark()==true ? Colors.white:Colors.black, // Cambia el color de la flecha a negro aquí
          ),
        title: Text("Your Mentorships",
          style: TextStyle(
            color: _lightSensor.isActivatedDark()==true ? Colors.white:Colors.black, // Se cambia el color del texto a negro
          ),),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
        backgroundColor: _lightSensor.isActivatedDark()==true ? Colors.blueGrey:Color(0xFFfaf2f2)

      ),
      body: Container(
        padding: const EdgeInsets.fromLTRB(10, 20, 10, 0),
        color: _lightSensor.isActivatedDark()==true ? Colors.black12:Color(0xFFD1EDBF),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                color: _lightSensor.isActivatedDark()==true ? Colors.blueGrey:Color(0xFFfaf2f2),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            Visibility(
              visible: widget.online == false,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "There is no internet connection, return to main page and please check connectivity",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 30,
                          color: _lightSensor.isActivatedDark()==true ? Colors.white:Color(0xFF000000),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),

            Visibility(
              visible: widget.online == false,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "It shows the mentorships you have enrolled while you have internet (in this phone)",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 30,
                          color: _lightSensor.isActivatedDark()==true ? Colors.white:Color(0xFF000000),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            if (uvm.isLoading)
              Padding(
                padding: const EdgeInsets.only(top: 32.0, bottom: 16.0),
                child: Center(
                  child: CircularProgressIndicator(
                    strokeWidth: 8.0, // aumenta el valor para un círculo más grande
                  ),
                ),
              ),
            Expanded (
              child: MentorshipList(mentorships: uvm.mentorshipsVM),
            )
          ],
        ),
      ),
    );
  }
}
