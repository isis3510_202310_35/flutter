import 'dart:async';

import 'package:flutter/material.dart';
import 'package:algolia/algolia.dart';
import 'package:tutorya/singletons/singletonConnectivity.dart';
import 'package:tutorya/singletons/singletonLightSensor.dart';
import 'package:tutorya/viewModels/globalUser_view_model.dart';
import 'package:tutorya/viewModels/mentorship_list_view_model.dart';
import 'package:tutorya/widgets/mentorship_list.dart';
import 'package:provider/provider.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class MentorshipListPage extends StatefulWidget {
  @override
  _MentorshipListPageState createState() => _MentorshipListPageState();
}

class _MentorshipListPageState extends State<MentorshipListPage> {
  final TextEditingController _controller = TextEditingController();
  final Algolia algolia = Algolia.init(
    applicationId: 'LGLFNSHD60',
    apiKey: '3a72104c2fa9376c2f45f814ba53bb4e',
  );
  Timer? _debounce;
  String _searchText="";
  String career="Systems Engineering";
  int _searchCount = 0; // Variable para almacenar el número de resultados
  bool isButtonVisibleReturn = false;

  // Connection
  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;
  String string = '';
  bool online=true;

  // DarkMode
  final LightSensor _lightSensor= LightSensor.instance;
  bool darkMode=false;

  @override
  void initState() {
    super.initState();
    _networkConnectivity.initialise();
    listenToNetworkConnectivity();
    Provider.of<GlobalUserViewModel>(context, listen: false).aumentarContadorClicks();
    if(online==true){
      career=Provider.of<GlobalUserViewModel>(context, listen: false).career;
      Provider.of<MentorshipListViewModel>(context, listen: false).fetchMentorshipsOfTop5Tutor(career);
      Provider.of<MentorshipListViewModel>(context, listen: false).getTop3Courses();
    }
  }

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<MentorshipListViewModel>(context);
    final uvm = Provider.of<GlobalUserViewModel>(context);
    return Scaffold(
      backgroundColor: _lightSensor.isActivatedDark()==true ? Colors.black12:Color(0xFFD1EDBF),
      appBar: AppBar(
        title: Text("Mentorships",
          style: TextStyle(
            color: _lightSensor.isActivatedDark()==true ? Colors.white:Colors.black, // Se cambia el color del texto a negro
          ),),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
        backgroundColor: _lightSensor.isActivatedDark()==true ? Colors.blueGrey:Color(0xFFfaf2f2),
        automaticallyImplyLeading: false,

      ),
      body: Container(
        padding: const EdgeInsets.fromLTRB(10, 20, 10, 0),
        color: _lightSensor.isActivatedDark()==true ? Colors.black12:Color(0xFFD1EDBF),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                color: _lightSensor.isActivatedDark()==true ? Colors.blueGrey:Color(0xFFfaf2f2),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.all(8),
                    child: Icon(
                      Icons.search,
                      color: _lightSensor.isActivatedDark()==true ? Colors.white:Colors.black,
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      controller: _controller,
                      onChanged: (value) async {
                        listenToNetworkConnectivity();
                        isButtonVisibleReturn = false;
                        if(value.length<_searchText.length){
                          setState(() {
                            _searchCount = 0; // Actualizar el número de resultados
                          });
                        }
                        setState(() {
                          _searchText = value;
                        });
                        if (value.isEmpty) {
                          clearSearch(vm);
                        } else {
                            vm.clearMentorships();
                            if(_searchText.length>=3){
                              if(online==true){
                                vm.clearMentorshipsShared();
                                setState(() {
                                  vm.lastSearch = value; // Actualizar el número de resultados
                                });
                              }
                              vm.isLoading=true;
                              AlgoliaQuery query = algolia.instance.index('prod_tutorya_mentorships').query(value);
                              AlgoliaQuerySnapshot querySnapshot = await query.getObjects();
                              List hits = querySnapshot.hits;
                              List objectIds = hits.map((hit) => hit.objectID).toList();
                              setState(() {
                                _searchCount = objectIds.length; // Actualizar el número de resultados
                              });
                              await vm.fetchMentorshipsByObjectIds(objectIds);
                              vm.fetchMentorshipsShared();
                            }
                        }
                      },
                      style: TextStyle(color: _lightSensor.isActivatedDark()==true ? Colors.white:Colors.black),
                      decoration: InputDecoration(
                        hintText: "Search (course or mentorship's name)",
                        hintStyle: TextStyle(color: _lightSensor.isActivatedDark()==true ? Colors.white:Color(0xFF000000)),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Visibility(
              visible: _searchText.isEmpty && online==true,
              child: Padding(
                padding: EdgeInsets.only(top: 20),
              child: Column(
                children: [
                  Text(
                    "Top 3 most clicked Courses",
                    style: TextStyle(
                      fontSize: 18,
                        color: _lightSensor.isActivatedDark()==true ? Colors.white:Colors.black
                    ),
                  ),
                  SizedBox(height: 10),
                  Wrap(
                    spacing: 10,
                    children: vm.top3Courses
                        .map((course) => ElevatedButton(
                      onPressed: () async {
                        uvm.aumentarContadorClicks();
                        vm.isLoading=true;
                        vm.clearMentorships();
                        listenToNetworkConnectivity();
                        if(online==true){
                          vm.clearMentorshipsShared();
                          setState(() {
                            vm.lastSearch = course.getname; // Actualizar el número de resultados
                          });
                        }
                        setState(() {
                          _searchText = course.getname;
                          isButtonVisibleReturn=true;
                        });
                        await vm.fetchMentorshipsbyCourse(_searchText);
                        setState(() {
                          _searchCount = vm.studentMentorshipsIdSearch.length; // Actualizar el número de resultados
                        });
                        vm.fetchMentorshipsShared();
                      },
                      child: Text(course.getname,
                        style: TextStyle(
                            color: _lightSensor.isActivatedDark()==true ? Colors.white:Colors.black
                        ),),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: _lightSensor.isActivatedDark()==true ? Colors.blueGrey:Color(0xFFfaf2f2), // Cambiar el color de fondo a azul
                      ),
                    ))
                        .toList(),
                  ),
                ],
              ),
            ),
            ),
            Visibility(
          visible: _searchText.isEmpty && online==true,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Center(
              child: Text(
                "Recommended tutors for your career based on the mentorships the other users took on $career: ",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                  color: _lightSensor.isActivatedDark()==true ? Colors.white:Color(0xFF000000),
                ),
              ),
            ),
          ),
        ),
            Visibility(
              visible: _searchText.length>=3 && online==true && vm.isLoading==false,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text(
                      _searchCount>0 ? '$_searchCount result(s) for "$_searchText"' : 'No results for "$_searchText"',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 18,
                      color: _lightSensor.isActivatedDark()==true ? Colors.white:Color(0xFF000000),
                    ),
                  ),
                ),
              ),
            ),
            Visibility(
              visible: _searchText.length<3 && !_searchText.isEmpty && online==true,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text(
                    "Please write at least 3 words",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 18,
                      color: _lightSensor.isActivatedDark()==true ? Colors.white:Color(0xFF000000),
                    ),
                  ),
                ),
              ),
            ),
            if (vm.isLoading && online==true)
              Padding(
                padding: const EdgeInsets.only(top: 32.0, bottom: 16.0),
                child: Center(
                  child: CircularProgressIndicator(
                    strokeWidth: 8.0, // aumenta el valor para un círculo más grande
                  ),
                ),
              ),
            Visibility(
              visible: online == false,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "There is no internet connection please check connectivity ",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 30,
                          color: _lightSensor.isActivatedDark()==true ? Colors.white:Color(0xFF000000),
                        ),
                      ),
                      ElevatedButton(
                        onPressed: () {
                          _refreshPage(vm);
                        },
                        child: Text(
                          'Refresh',
                          style: TextStyle(fontSize: 18,
                              color: _lightSensor.isActivatedDark()==true ? Colors.white:Color(0xFF000000)),
                        ),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: _lightSensor.isActivatedDark()==true ? Colors.blueGrey:Color(0xFFfaf2f2), // Cambiar el color de fondo a azul
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Visibility(
              visible: online == false && vm.lastSearch.isNotEmpty,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "The results for your latest search '${vm.lastSearch}' were: ",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 18,
                          color: _lightSensor.isActivatedDark()==true ? Colors.white:Color(0xFF000000),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Visibility(
              visible: isButtonVisibleReturn && online==true,
              child: ElevatedButton(
                onPressed: () {
                  uvm.aumentarContadorClicks();
                  vm.clearMentorships();
                  setState(() {
                    _searchText = "";
                    isButtonVisibleReturn=false;
                    _searchCount = 0;
                  });
                },
                child: Text(
                  'Return',
                  style: TextStyle(fontSize: 18,
                      color: _lightSensor.isActivatedDark()==true ? Colors.white:Color(0xFF000000)),
                ),
                style: ElevatedButton.styleFrom(
                  backgroundColor: _lightSensor.isActivatedDark()==true ? Colors.blueGrey:Color(0xFFfaf2f2), // Cambiar el color de fondo a azul
                ),
              ),
            ),
            Expanded(
              child: online==true
                  ? (_searchText.isEmpty? MentorshipList(mentorships: vm.studentMentorships):MentorshipList(mentorships: vm.studentMentorshipsIdSearch))
                  : MentorshipList(mentorships: vm.mentorshipsShared),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _debounce?.cancel();
    super.dispose();
  }

  Future<void> clearSearch(MentorshipListViewModel vm) async {
    setState(() {
      _searchText = '';
      _searchCount = 0;
      _controller.text='';
    });
    vm.clearMentorships();
    listenToNetworkConnectivity();
    if(online==true){
      vm.fetchMentorshipsOfTop5Tutor(career);
    }
  }

  Future<void> listenToNetworkConnectivity() async {
    // print("Entramos a revisar");
    await _networkConnectivity.myStream.listen((source) {
      _source = source;
      // print('source $_source');
      switch (_source.keys.toList()[0]) {
        case ConnectivityResult.mobile:
          string = _source.values.toList()[0] ? 'Mobile: Online' : 'Mobile: Offline';
          if (string.compareTo('Mobile: Offline') == 0) {
            online = false;
          }
          else online=true;
          break;
        case ConnectivityResult.wifi:
          string = _source.values.toList()[0] ? 'WiFi: Online' : 'WiFi: Offline';
          if (string.compareTo('WiFi: Offline') == 0) {
            online = false;
          }
          else online=true;
          break;
        case ConnectivityResult.none:
        default:
          string = 'No internet connection';
          online = false;
      }
      // print(string);
    });
  }

  void _refreshPage(MentorshipListViewModel vm) {
    listenToNetworkConnectivity();
    clearSearch(vm);
    isButtonVisibleReturn=false;
  }

}
