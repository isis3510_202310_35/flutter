import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tutorya/singletons/singletonConnectivity.dart';
import 'package:tutorya/viewModels/globalUser_view_model.dart';
import 'package:tutorya/viewModels/mentorship_list_view_model.dart';
import 'package:tutorya/viewModels/mentorship_view_model.dart';
import 'package:tutorya/singletons/singletonLightSensor.dart';

class MentorshipDetailDialog extends StatefulWidget {
  final MentorshipViewModel mentorship;

  MentorshipDetailDialog({required this.mentorship});

  @override
  _MentorshipDetailDialogState createState() => _MentorshipDetailDialogState();
}

class _MentorshipDetailDialogState extends State<MentorshipDetailDialog> {
  final LightSensor _lightSensor = LightSensor.instance;

  // Connection
  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;
  String string = '';
  bool online=true;

  @override
  void initState() {
    super.initState();
    _networkConnectivity.initialise();
    listenToNetworkConnectivity();
  }

  @override
  Widget build(BuildContext context) {
    final uvm = Provider.of<GlobalUserViewModel>(context);
    final vm = Provider.of<MentorshipListViewModel>(context);
    return Scaffold(
      backgroundColor: _lightSensor.isActivatedDark()==true ? Colors.black12:Color(0xFFD1EDBF),
      appBar: AppBar(
        backgroundColor: _lightSensor.isActivatedDark()==true ? Colors.blueGrey:Color(0xFFfaf2f2),
        iconTheme: IconThemeData(
          color: _lightSensor.isActivatedDark()==true ? Colors.white:Colors.black, // Cambia el color de la flecha a negro aquí
        ),
        title: Text(
          widget.mentorship.name,
          style: TextStyle(
            color: _lightSensor.isActivatedDark()==true ? Colors.white:Colors.black,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(16.0), // Agrega un margen interno alrededor del contenido
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 300,
                  width: 300,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.contain,
                      image: NetworkImage(widget.mentorship.photo),
                    ),
                  ),
                ),
                SizedBox(height: 16.0),
                Text(
                  'Tutor:\n${widget.mentorship.tutorName}\n',
                  style: TextStyle(
                    color: _lightSensor.isActivatedDark() == true ? Colors.white : Color(0xFF000000),
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 8.0),
                Text(
                  'Description:\n${widget.mentorship.description}\n',
                  style: TextStyle(
                    color: _lightSensor.isActivatedDark() == true ? Colors.white : Color(0xFF000000),
                    fontSize: 14.0,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 8.0),
                Text(
                  'Date:\n${DateFormat('MMM d (E) H:mm').format(widget.mentorship.date)}\n',
                  style: TextStyle(
                    color: _lightSensor.isActivatedDark() == true ? Colors.white : Color(0xFF000000),
                    fontSize: 14.0,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 8.0),
                Text(
                  'Duration:\n${widget.mentorship.mentorship.duration} hour(s)',
                  style: TextStyle(
                    color: _lightSensor.isActivatedDark() == true ? Colors.white : Color(0xFF000000),
                    fontSize: 14.0,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 8.0),
                Text(
                  'Capacity:\n${widget.mentorship.mentorship.capacity} person(s), actual: ${widget.mentorship.getActualCapacity}',
                  style: TextStyle(
                    color: _lightSensor.isActivatedDark() == true ? Colors.white : Color(0xFF000000),
                    fontSize: 14.0,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 16.0),
                ElevatedButton(
                  child: Text(
                    uvm.alreadyEnrolled(widget.mentorship.id) ? 'Enrolled' : (widget.mentorship.getActualCapacity==widget.mentorship.mentorship.capacity)? "Full":'Enroll',
                    style: TextStyle(
                      color: _lightSensor.isActivatedDark() ? Colors.white : Colors.black,
                    ),
                  ),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                      _lightSensor.isActivatedDark() ? Colors.blueGrey : (uvm.alreadyEnrolled(widget.mentorship.id) || (widget.mentorship.getActualCapacity==widget.mentorship.mentorship.capacity)) ?Color(0xFFd1c5c5):Color(0xFFfaf2f2),
                    ),
                  ),
                  onPressed: () async {
                    await listenToNetworkConnectivity(); // Ejecutar el método 'a()'
                    if(online==false){
                      showDialog<String> (
                          context: context,
                          builder: (BuildContext context) => AlertDialog (
                              backgroundColor: Colors.white, // Cambiar el fondo a blanco
                              title: const Text("No internet connection",
                                  style: TextStyle(color: Colors.black)),
                              content: const Text('To enroll to a mentorship, please make sure to have an internet connection.',
                                style: TextStyle(color: Colors.black),),
                              actions: <Widget>[
                                TextButton(
                                    child: const Text('Ok'),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    }
                                )
                              ]
                          )
                      );
                    }
                    else if (uvm.alreadyEnrolled(widget.mentorship.id) || (widget.mentorship.getActualCapacity == widget.mentorship.mentorship.capacity)) {
                      print("O esta lleno o esta enrolado");
                    } else {
                      setState(() {
                        widget.mentorship.sumActualCapacity();
                      });
                      uvm.addMentorshipToUser(widget.mentorship.mentorship);
                      vm.addUserToMentorship(uvm.id, widget.mentorship.mentorship);
                      uvm.insertAndUpdateClick();
                    }
                  },
                ),

            ElevatedButton(
              child: Text(
                'Close',
                style: TextStyle(color: _lightSensor.isActivatedDark()==true ? Colors.white:Colors.black)
              ),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(_lightSensor.isActivatedDark()==true ? Colors.blueGrey:Color(0xFFfaf2f2)), // Cambia el color del botón aquí
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> listenToNetworkConnectivity() async {
    // print("Entramos a revisar");
    await _networkConnectivity.myStream.listen((source) {
      _source = source;
      // print('source $_source');
      switch (_source.keys.toList()[0]) {
        case ConnectivityResult.mobile:
          string = _source.values.toList()[0] ? 'Mobile: Online' : 'Mobile: Offline';
          if (string.compareTo('Mobile: Offline') == 0) {
            online = false;
          }
          else online=true;
          break;
        case ConnectivityResult.wifi:
          string = _source.values.toList()[0] ? 'WiFi: Online' : 'WiFi: Offline';
          if (string.compareTo('WiFi: Offline') == 0) {
            online = false;
          }
          else online=true;
          break;
        case ConnectivityResult.none:
        default:
          string = 'No internet connection';
          online = false;
      }
      // print(string);
    });
  }

}