import 'package:flutter/material.dart';
import 'package:tutorya/viewModels/tutor_list_view_model.dart';
import 'package:tutorya/widgets/tutor_list.dart';
import 'package:provider/provider.dart';

class TutorListView extends StatefulWidget {
  @override
  TutorListViewState createState() => TutorListViewState();
}

class TutorListViewState extends State<TutorListView> {
  @override
  void initState() {
    super.initState();
    Provider.of<TutorListViewModel>(context, listen: false).fetchTutors();
  }

  @override
  Widget build(BuildContext context) {
    final tvm = Provider.of<TutorListViewModel>(context);
    return Scaffold (
      body: Container (
        padding: EdgeInsets.all(10),
        child: Column (
          children: <Widget> [
            Expanded (
              child: TutorList(tutors: tvm.tutors)
            )
          ]
        )
      )
    );
  }
}
