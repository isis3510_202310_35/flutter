import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:tutorya/services/userservice.dart';
import 'package:provider/provider.dart';
import 'package:tutorya/singletons/singletonLightSensor.dart';
import 'package:tutorya/viewModels/globalUser_view_model.dart';
import '../../widgets/profile_pic.dart';
import 'package:tutorya/models/mentorship.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenView createState() => _ProfileScreenView();
}

int getAve(List<Mentorship> mentorships) {
  var re = 0.0;
  for(int i = 0; i < mentorships.length; i++) {
    var realDate = DateTime.parse(mentorships[i].date);
    var now = DateTime.now();
    if(realDate.isAfter(now.subtract(const Duration(days: 84))) && realDate.isBefore(now)) {
      re++;
    }
  }
  re = re / 12;
  return re.round();
}

class _ProfileScreenView extends State<ProfileScreen> {
  final LightSensor _lightSensor= LightSensor.instance;
  File? userImage;
  String? userName;
  int ave = 1;

  Future _getImage(String id) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      imageQuality: 50,
    );

    setState(() {
      if (pickedFile != null) {
        userImage = File(pickedFile.path);
        _uploadImageToFirebase(id);
      } else {
        print('No image selected.');
      }
    });
  }

  Future<void> _uploadImageToFirebase(String id) async {
    if (userImage != null) {
      try {
        final Reference firebaseStorageRef = FirebaseStorage.instance
            .ref()
            .child('userImages/${DateTime.now().millisecondsSinceEpoch}');
        await firebaseStorageRef.putFile(userImage!);
        final imageUrl = await firebaseStorageRef.getDownloadURL();
        print('Imagen subida a Firebase Storage: $imageUrl');
        UserService().updateUserPhoto(id, imageUrl);
        Provider.of<GlobalUserViewModel>(context, listen: false)
            .setPhoto(imageUrl);
      } on FirebaseException catch (e) {
        print(e);
      }
    } else {
      print('No hay imagen para subir.');
    }
  }

  Future<void> setAve(List<Mentorship> ments) async {
    ave = await compute(getAve, ments);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final uvm = Provider.of<GlobalUserViewModel>(context, listen: true);
    setAve(uvm.mentorships);
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
              top: 0,
              left: 0,
              right: 0,
              height: MediaQuery.of(context).size.height * 0.4,
              child: Container(
                color: _lightSensor.isActivatedDark()==true ? Colors.black12:Color.fromRGBO(209, 237, 191, 1),
                child: Center(
                  child: GestureDetector(
                    onTap: () => _getImage(uvm.id),
                    child: ProfilePic (
                      img_url: uvm.photo,
                      size: 100,
                    ),
                  ),
                ),
              )),
          Positioned(
            bottom: -16.0,
            left: 0,
            right: 0,
            height: MediaQuery.of(context).size.height * 0.6 + 50.0,
            child: Container(
              decoration: BoxDecoration(
                color: _lightSensor.isActivatedDark()==true ? Colors.grey:Colors.white,
                borderRadius: BorderRadius.vertical(top: Radius.circular(12.0)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    spreadRadius: 2,
                    blurRadius: 10,
                    offset: Offset(0, 2),
                  ),
                ],
              ),
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              child: Column(
                children: [
                  SizedBox(
                    height: 16.0,
                  ),
                  Text(
                    Provider.of<GlobalUserViewModel>(context, listen: false)
                        .name!,
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: _lightSensor.isActivatedDark()==true ? Colors.white:Color.fromRGBO(0, 0, 0, 1)),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 1,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: _lightSensor.isActivatedDark()==true ? Colors.blueGrey:Color.fromRGBO(255, 231, 171, 1),
                      ),
                      onPressed: () => _getImage(uvm.id),
                      child: Text(
                        'Change Picture',
                        style: TextStyle(color: _lightSensor.isActivatedDark()==true ? Colors.white:Color.fromRGBO(36, 40, 44, 1)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 1,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: _lightSensor.isActivatedDark()==true ? Colors.blueGrey:Color.fromRGBO(255, 231, 171, 1),
                      ),
                      onPressed: () => Navigator.pushNamed(context, '/settings'),
                      child: Text(
                        'Go to Settings',
                        style: TextStyle(color: _lightSensor.isActivatedDark()==true ? Colors.white:Color.fromRGBO(36, 40, 44, 1)),
                      ),
                    ),
                  ),
                  SizedBox(height: 16.0),
                  uvm.type == 1 ?
                    Text (
                      "Average number of mentorships per week in the past 3 months: ${ave}",
                      style: TextStyle (
                        color: _lightSensor.isActivatedDark()==true ? Colors.white:Color(0xFF000000),
                        fontSize: 40,
                      ),
                    ) :
                    Text("You are a student!",
                      style: TextStyle (
                        color: _lightSensor.isActivatedDark()==true ? Colors.white:Color(0xFF000000),
                        fontSize: 40,
                      ),),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
