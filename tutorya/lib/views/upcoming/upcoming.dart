import 'package:flutter/material.dart';
import 'package:tutorya/viewModels/globalUser_view_model.dart';
import 'package:tutorya/viewModels/mentorship_list_view_model.dart';
import 'package:tutorya/widgets/mentorship_list.dart';
import 'package:provider/provider.dart';

class Upcoming extends StatefulWidget {
  @override
  _Upcoming createState() => _Upcoming();
}

class _Upcoming extends State<Upcoming> {
  final String name = "Leo Messi";
  @override
  void  initState() {
    super.initState();
    Provider.of<GlobalUserViewModel>(context, listen: false).aumentarContadorClicks();
    Provider.of<MentorshipListViewModel>(context, listen: false).fetchUpcomingMentorshipsFor(name);
  }

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<MentorshipListViewModel>(context);
    return Scaffold (
      appBar: AppBar (
        title: const Text('Upcoming Mentorships'),
      ),
      body: Flex (
        direction: Axis.vertical,
        children: <Widget> [ 
          vm.mentorships.isNotEmpty ? 
          Expanded (
            child: MentorshipList(mentorships: vm.mentorships),
          )
          :
          const Padding (
            padding: const EdgeInsets.only(top: 150.0),
            child: const Text (
              r"No upcoming mentorships ¯\_(ツ)_/¯",
              textAlign: TextAlign.center,
              style: const TextStyle(fontSize: 40),
            ),
          )
        ]
      ),
    );
  }
}
