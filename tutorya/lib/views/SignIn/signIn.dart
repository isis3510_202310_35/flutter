import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tutorya/blocs/user/bloc/user_bloc.dart';
import 'package:tutorya/models/userApp.dart';
import 'package:tutorya/services/userservice.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:flutter/services.dart';
import 'package:tutorya/services/utils.dart';
import 'package:tutorya/singletons/singletonConnectivity.dart';

class RegistrationScreen extends StatefulWidget {
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _formKey = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _phoneNumberController = TextEditingController();
  final _semesterController = TextEditingController();
  String _careerSelected = 'Systems Engineering';
  String? _typeSelected;

  List<String> _options = [
    'Systems Engineering',
    'Mechanical Engineering',
    'Laws',
    'Biology',
  ];

  bool _acceptedTermsAndConditions = false;

  // Connection
  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;
  bool online = true;

  @override
  void initState() {
    super.initState();
    _networkConnectivity.initialise();
    listenToNetworkConnectivity();
  }

  @override
  void dispose() {
    _passwordController.dispose();
    _emailController.dispose();
    _firstNameController.dispose();
    _lastNameController.dispose();
    _phoneNumberController.dispose();
    _semesterController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color.fromRGBO(36, 40, 44, 1),
        onPressed: () {
          Navigator.pop(context);
        },
        child: Icon(Icons.arrow_back, color: Color.fromRGBO(255, 255, 255, 1)),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
      body: Container(
        height: MediaQuery.of(context).size.height * 1,
        color: Color.fromRGBO(209, 237, 191, 1),
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 50),
                  child: Center(
                    child: BlocBuilder<UserBloc, UserState>(
                      builder: (context, state) {
                        return Text(
                          "Registration",
                          style: TextStyle(color: Color(0xFF000000)),
                        );
                      },
                    ),
                  ),
                ),
                SizedBox(height: 16),
                TextFormField(
                  controller: _emailController,
                  style: TextStyle(color: Colors.black),
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    labelText: "Email",
                    hintText: "Email",
                    hintStyle: TextStyle(color: Colors.black38),
                    filled: true,
                    fillColor: Color.fromRGBO(250, 244, 242, 1),
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color.fromARGB(0, 0, 0, 0)),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter an email address';
                    }
                    if (!RegExp(r'\S+@\S+\.\S+').hasMatch(value)) {
                      return 'Please enter a valid email address';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16),
                TextFormField(
                  controller: _passwordController,
                  style: TextStyle(color: Colors.black),
                  obscureText: true,
                  decoration: InputDecoration(
                    labelText: "Password",
                    hintText: "Password",
                    hintStyle: TextStyle(color: Colors.black38),
                    filled: true,
                    fillColor: Color.fromRGBO(250, 244, 242, 1),
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color.fromARGB(0, 0, 0, 0)),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  validator: (value) {
                    final RegExp regex = RegExp(
                        r'^(?=.*[a-z])(?=.*[A-Z])(?=.*[-_])[a-zA-Z0-9-_]{8,}$');
                    if (value!.isEmpty) {
                      return 'Please enter a password';
                    } else if (value.length < 8) {
                      return 'Password must be at least 8 characters long';
                    } else if (!regex.hasMatch(value)) {
                      return 'Please enter a password with at least 1 lowercase letter, 1 uppercase letter, 1 "-" or "_" character and minimum of 8 characters';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16),
                TextFormField(
                  controller: _firstNameController,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    labelText: "First Name",
                    hintText: "First Name",
                    hintStyle: TextStyle(color: Colors.black38),
                    filled: true,
                    fillColor: Color.fromRGBO(250, 244, 242, 1),
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color.fromARGB(0, 0, 0, 0)),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter your first name';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16),
                TextFormField(
                  controller: _lastNameController,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    labelText: "Last Name",
                    hintText: "Last Name",
                    hintStyle: TextStyle(color: Colors.black38),
                    filled: true,
                    fillColor: Color.fromRGBO(250, 244, 242, 1),
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color.fromARGB(0, 0, 0, 0)),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter your last name';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16),
                TextFormField(
                  controller: _phoneNumberController,
                  keyboardType: TextInputType.phone,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    labelText: "Phone Number",
                    hintText: "Phone Number",
                    hintStyle: TextStyle(color: Colors.black38),
                    filled: true,
                    fillColor: Color.fromRGBO(250, 244, 242, 1),
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color.fromARGB(0, 0, 0, 0)),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter a phone number';
                    }
                    if (value.length != 10) {
                      return 'Please enter a valid 10-digit phone number';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16),
                TextFormField(
                  controller: _semesterController,
                  keyboardType: TextInputType.number,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    labelText: "Semester",
                    hintText: "Semester",
                    hintStyle: TextStyle(color: Colors.black38),
                    filled: true,
                    fillColor: Color.fromRGBO(250, 244, 242, 1),
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color.fromARGB(0, 0, 0, 0)),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter semester';
                    } else if (int.parse(value) <= 0 ||
                        int.parse(value) >= 20) {
                      return 'Please enter a valid semester';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16),
                DropdownButtonFormField(
                  dropdownColor: Color.fromRGBO(250, 244, 242, 1),
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Color.fromRGBO(250, 244, 242, 1),
                    labelText: "Select your main career",
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color.fromARGB(0, 0, 0, 0)),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  icon: Icon(Icons.arrow_drop_down, color: Colors.black),
                  value: _careerSelected,
                  onChanged: (value) {
                    setState(() {
                      _careerSelected = value as String;
                    });
                  },
                  items: _options.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value, style: TextStyle(color: Colors.black)),
                    );
                  }).toList(),
                ),
                SizedBox(height: 16),
                DropdownButtonFormField<String>(
                  value: _typeSelected,
                  dropdownColor: Color.fromRGBO(250, 244, 242, 1),
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Color.fromRGBO(250, 244, 242, 1),
                    labelText: "What are you?",
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color.fromARGB(0, 0, 0, 0)),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  icon: Icon(Icons.arrow_drop_down, color: Colors.black),
                  items: [
                    DropdownMenuItem(
                      value: '1',
                      child:
                          Text('Mentor', style: TextStyle(color: Colors.black)),
                    ),
                    DropdownMenuItem(
                      value: '2',
                      child: Text('Student',
                          style: TextStyle(color: Colors.black)),
                    ),
                  ],
                  onChanged: (value) {
                    setState(() {
                      _typeSelected = value;
                    });
                  },
                ),
                Theme(
                  data: ThemeData(unselectedWidgetColor: Colors.white),
                  child: CheckboxListTile(
                    title: Text('Agree to the terms and conditions',
                        style: TextStyle(color: Colors.black)),
                    value: _acceptedTermsAndConditions,
                    onChanged: (value) {
                      setState(() {
                        _acceptedTermsAndConditions = value!;
                      });
                    },
                    controlAffinity: ListTileControlAffinity.leading,
                    selected: _acceptedTermsAndConditions,
                    activeColor: Colors.green,
                    checkColor: Colors.grey[850],
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: 50.0,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Color.fromRGBO(36, 40, 44, 1),
                    ),
                    onPressed: () async {
                      listenToNetworkConnectivity();
                      if (_formKey.currentState!.validate() &&
                          _acceptedTermsAndConditions) {
                        if (online == true) {
                          submitRegistrationData();
                        } else {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text('No internet connection.',
                                    style: TextStyle(
                                        color: Color.fromRGBO(36, 40, 44, 1))),
                                backgroundColor:
                                    Color.fromRGBO(255, 231, 171, 1),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                content: Text(
                                    'Please check your connection and try again.',
                                    style: TextStyle(
                                        color: Color.fromRGBO(36, 40, 44, 1))),
                                actions: <Widget>[
                                  TextButton(
                                    child: Text('OK'),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              );
                            },
                          );
                        }
                      }
                    },
                    child: Text('Register'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> submitRegistrationData() async {
    // Valores de los campos de formulario
    String email = _emailController.text;
    String password = _passwordController.text;
    String firstName = _firstNameController.text;
    String lastName = _lastNameController.text;
    String phoneNumber = _phoneNumberController.text;
    String semester = _semesterController.text;
    String career = _careerSelected;
    String? userType = _typeSelected;

    final data = UserApp(
        id: '',
        name: '$firstName $lastName',
        email: email,
        phone: phoneNumber,
        photo:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Default_pfp.svg/1200px-Default_pfp.svg.png',
        type: int.parse(userType!),
        points: 0,
        password: await encryptPasswordCompute(password),
        career: career,
        semester: int.parse(semester),
        rate: 0
    );

    UserService()
        .getUserByEmail(email)
        .then((value) => {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text('Error!',
                        style: TextStyle(color: Color.fromRGBO(36, 40, 44, 1))),
                    backgroundColor: Color.fromRGBO(255, 231, 171, 1),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    content: Text('An account with that email already exists.',
                        style: TextStyle(color: Color.fromRGBO(36, 40, 44, 1))),
                    actions: [
                      TextButton(
                        child: Text('OK',
                            style: TextStyle(
                                color: Color.fromRGBO(36, 40, 44, 1))),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              ),
            })
        .catchError((error) => {
              if (error.toString() == "User not found")
                UserService().createUser(data).then((value) => {
                      Navigator.pop(context),
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text('User created successfully',
                                style: TextStyle(
                                    color: Color.fromRGBO(36, 40, 44, 1))),
                            backgroundColor: Color.fromRGBO(255, 231, 171, 1),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            content: Text('The user was created successfully.',
                                style: TextStyle(
                                    color: Color.fromRGBO(36, 40, 44, 1))),
                            actions: [
                              TextButton(
                                child: Text('OK',
                                    style: TextStyle(
                                        color: Color.fromRGBO(36, 40, 44, 1))),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      ),
                    })
            });
  }

  void _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Cuenta creada"),
          content: Text("¡Tu cuenta ha sido creada exitosamente!"),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text("OK"),
            ),
          ],
        );
      },
    );
  }

  Future<void> listenToNetworkConnectivity() async {
    // print('Esta entrando');
    await _networkConnectivity.myStream.listen((source) {
      // print(['Esta entrando 2', source]);
      _source = source;
      switch (_source.keys.toList()[0]) {
        case ConnectivityResult.mobile:
          _source.values.toList()[0] ? online = true : online = false;
          break;
        case ConnectivityResult.wifi:
          _source.values.toList()[0] ? online = true : online = false;
          break;
        case ConnectivityResult.none:
        default:
          online = false;
      }
    });
  }
}
