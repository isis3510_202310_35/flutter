import 'dart:io';
import 'dart:core';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:tutorya/blocs/user/bloc/user_bloc.dart';
import 'package:tutorya/services/userservice.dart';
import 'package:tutorya/singletons/singletonConnectivity.dart';
import 'package:tutorya/singletons/singletonLightSensor.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:provider/provider.dart';
import 'package:tutorya/viewModels/globalUser_view_model.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';

class LogInScreen extends StatefulWidget {
  @override
  _LogInScreenState createState() => _LogInScreenState();
}

class _LogInScreenState extends State<LogInScreen> {
  final LocalAuthentication auth = LocalAuthentication();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool isLoading = false;
  bool incorrectPassword = false;
  bool incorrectUsername = false;
  final _formKey = GlobalKey<FormState>();
  bool _rememberCredentials = false;
  final LightSensor _lightSensor = LightSensor.instance;
  late DateTime startTime;

  // Connection
  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;
  bool online = true;

  @override
  void initState() {
    super.initState();
    _lightSensor.initPlatformState();
    _networkConnectivity.initialise();
    listenToNetworkConnectivity();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    startTime = DateTime.now();
    if (online == true) _authenticate().then((value) => handleLogIn());
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void handleLogIn() {
    setState(() {
      isLoading = true;
    });

    UserService()
        .getUserByEmail(_emailController.text)
        .then((value) => {
              if (value.password == encryptPassword(_passwordController.text))
                {
                  incorrectPassword = false,
                  if (_rememberCredentials == true)
                    {
                      escribirArchivo(
                          _emailController.text, _passwordController.text)
                    }
                  else
                    {escribirArchivo('', '')},
                  BlocProvider.of<UserBloc>(context).add(LogInUserEvent(value)),
                  Provider.of<GlobalUserViewModel>(context, listen: false)
                      .setUser(value.id, value.career, value.name, value.photo,
                          value.type, value.mentorships, startTime)
                      .then((value2) => {
                            setState(() {
                              isLoading = false;
                            }),
                            Navigator.pushNamed(context, '/home')
                          }),
                }
              else
                {
                  setState(() {
                    isLoading = false;
                    incorrectPassword = true;
                  }),
                  _formKey.currentState!.validate()
                }
            })
        .catchError((onError) => {
              if (onError.toString() == "User not found")
                {incorrectUsername = true, _formKey.currentState!.validate()},
              setState(() {
                isLoading = false;
              }),
            });
  }

  String encryptPassword(String password) {
    var bytes = utf8.encode(password); // convierte a formato utf8
    var hash = sha256.convert(bytes); // encripta
    return hash.toString(); // convierte a hexadecimal
  }

  @override
  Widget build(BuildContext context) {
    final uvm = Provider.of<GlobalUserViewModel>(context);
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      body: Form(
        key: _formKey,
        child: Stack(
          children: [
            // Contenedor superior
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              height: mediaQuery.size.height * 0.55,
              child: Container(
                color: const Color.fromRGBO(209, 237, 191, 1),
                child: Center(
                  child: Image.asset(
                    'assets/images/tutorYaIcon.png',
                    width: mediaQuery.size.width * 0.8,
                  ),
                ),
              ),
            ),
            // Contenedor inferior
            Positioned(
              bottom: -16.0,
              left: 0,
              right: 0,
              height: mediaQuery.size.height * 0.45 + 100.0,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(12.0)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 2,
                      blurRadius: 10,
                      offset: Offset(0, 2),
                    ),
                  ],
                ),
                padding: EdgeInsets.symmetric(horizontal: 24.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextFormField(
                      controller: _emailController,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Email cannot be empty';
                        } else if (incorrectUsername == true) {
                          return 'Email not found';
                        }
                        return null;
                      },
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Color.fromRGBO(250, 244, 242, 1),
                        labelText: "Email",
                        hintText: "Email",
                        hintStyle: TextStyle(color: Colors.black38),
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color.fromARGB(0, 0, 0, 0)),
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey),
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                      ),
                    ),
                    SizedBox(height: 16.0),
                    TextFormField(
                      controller: _passwordController,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Password cannot be empty';
                        } else if (incorrectPassword == true) {
                          return 'Password is incorrect';
                        }
                        return null;
                      },
                      obscureText: true,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        hintText: 'Password',
                        filled: true,
                        fillColor: Color.fromRGBO(250, 244, 242, 1),
                        labelText: "Password",
                        hintStyle: TextStyle(color: Colors.black38),
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color.fromARGB(0, 0, 0, 0)),
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey),
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                      ),
                    ),
                    Theme(
                      data: ThemeData(unselectedWidgetColor: Colors.grey),
                      child: CheckboxListTile(
                        title: Text('Remember credentials',
                            style: TextStyle(
                                color: Color.fromRGBO(36, 40, 44, 1))),
                        value: _rememberCredentials,
                        onChanged: (value) {
                          setState(() {
                            _rememberCredentials = value!;
                          });
                        },
                        controlAffinity: ListTileControlAffinity.leading,
                        selected: _rememberCredentials,
                        activeColor: Colors.green,
                        checkColor: Colors.grey[850],
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    SizedBox(
                      width: mediaQuery.size.width * 0.5,
                      height: 50.0,
                      child: ElevatedButton(
                        onPressed: () async {
                          uvm.aumentarContadorClicks();
                          listenToNetworkConnectivity();
                          incorrectUsername = false;
                          incorrectPassword = false;
                          if (_formKey.currentState!.validate()) {
                            if (online) {
                              handleLogIn();
                            } else {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text('No internet connection.',
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(36, 40, 44, 1))),
                                    backgroundColor:
                                        Color.fromRGBO(255, 231, 171, 1),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    content: Text(
                                        'Please check your internet connection and try again.',
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(36, 40, 44, 1))),
                                    actions: <Widget>[
                                      TextButton(
                                        child: Text('OK'),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  );
                                },
                              );
                              print(['No connection', online]);
                            }
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color.fromRGBO(36, 40, 44, 1),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: 16.0),
                          child: isLoading
                              ? CircularProgressIndicator(
                                  color: Colors.white,
                                )
                              : Text(
                                  "Log in",
                                ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 32.0,
                      child: Center(
                        child: Text(
                          'Or',
                          style: TextStyle(
                              color: Color.fromRGBO(36, 40, 44, 1),
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: mediaQuery.size.width * 0.5,
                      height: 50.0,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Color.fromRGBO(255, 231, 171, 1),
                            foregroundColor: Color.fromRGBO(36, 40, 44, 1)),
                        onPressed: () {
                          Navigator.pushNamed(context, '/register');
                        },
                        child: Text('Sign Up'),
                      ),
                    ),
                    SizedBox(height: 32.0),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: !online
          ? FloatingActionButton(
              backgroundColor: Colors.red[300],
              foregroundColor: Colors.white,
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text('No internet connection.',
                          style:
                              TextStyle(color: Color.fromRGBO(36, 40, 44, 1))),
                      backgroundColor: Color.fromRGBO(255, 231, 171, 1),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      content: Text(
                          "You don't have internet connection. To access the application internet connection is a must, check your connection!",
                          style:
                              TextStyle(color: Color.fromRGBO(36, 40, 44, 1))),
                      actions: <Widget>[
                        TextButton(
                          child: Text('OK'),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );
              },
              child: Icon(Icons.warning),
            )
          : null,
    );
  }

  Future<void> listenToNetworkConnectivity() async {
    print('Esta entrando');
    await _networkConnectivity.myStream.listen((source) {
      print(['Esta entrando 2', source]);
      _source = source;
      switch (_source.keys.toList()[0]) {
        case ConnectivityResult.mobile:
          _source.values.toList()[0] ? online = true : online = false;
          break;
        case ConnectivityResult.wifi:
          _source.values.toList()[0] ? online = true : online = false;
          break;
        case ConnectivityResult.none:
        default:
          online = false;
      }
    });
  }

  Future<File> escribirArchivo(String string1, String string2) async {
    final file = await _getLocalFile();
    return file.writeAsString('$string1;$string2');
  }

  Future<List<String>> leerArchivo() async {
    try {
      final file = await _getLocalFile();
      final contents = await file.readAsString();
      return contents.split(';');
    } catch (e) {
      print('Error al leer el archivo: $e');
      throw Exception('Error al leer el archivo');
    }
  }

  Future<File> _getLocalFile() async {
    final directory = await getApplicationDocumentsDirectory();
    return File('${directory.path}/datos.txt');
  }

  Future<void> _authenticate() async {
    bool authenticated = false;
    try {
      authenticated = await auth.authenticate(
        localizedReason: 'Please authenticate to log in',
      );
    } catch (e) {
      print('Error: $e');
    }

    if (authenticated) {
      // Autenticado correctamente, leer archivo
      final data = await leerArchivo();
      _emailController.text = data[0];
      _passwordController.text = data[1];
      if (data[0] != '') {
        _rememberCredentials = true;
      }
    } else {
      // No autenticado, no leer archivo
    }
  }
}
