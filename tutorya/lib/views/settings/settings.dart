import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider/provider.dart';
import 'package:tutorya/services/userservice.dart';
import 'package:tutorya/services/utils.dart';
import 'package:tutorya/singletons/singletonConnectivity.dart';
import 'package:tutorya/viewModels/globalUser_view_model.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;
  Map _source = {ConnectivityResult.none: false};
  bool online = true;
  String currentPhoneNumber = "";
  String currentSemester = "";
  String newPhoneNumber = "";
  String newSemester = "";
  String newPassword = "";
  String currentPassword = "";

  @override
  void initState() {
    super.initState();
    _networkConnectivity.initialise();
    listenToNetworkConnectivity();

    fetchData();
  }

  void fetchData() async {
    final uvm = Provider.of<GlobalUserViewModel>(context, listen: false);

    final userId = uvm.id;
    print({'Data', uvm.id});

    final userDoc = FirebaseFirestore.instance.collection('users').doc(userId);
    final userSnapshot = await userDoc.get();

    final data = userSnapshot.data();

    setState(() {
      currentPhoneNumber = data?['phone'].toString() ?? '';
      currentSemester = data?['semester'].toString() ?? '';
    });
  }

  Future<void> listenToNetworkConnectivity() async {
    _networkConnectivity.myStream.listen((source) {
      _source = source;
      switch (_source.keys.toList()[0]) {
        case ConnectivityResult.mobile:
          _source.values.toList()[0] ? online = true : online = false;
          break;
        case ConnectivityResult.wifi:
          _source.values.toList()[0] ? online = true : online = false;
          break;
        case ConnectivityResult.none:
        default:
          online = false;
      }
    });
  }

  void saveChanges() {
    if (newPassword.isNotEmpty) {
      changePassword();
    }
    if (newPhoneNumber.isNotEmpty) {
      changePhoneNumber();
    }
    if (newSemester.isNotEmpty) {
      changeSemester();
    }
  }

  Future<void> changePassword() async {
    final uvm = Provider.of<GlobalUserViewModel>(context, listen: false);
    final userId = uvm.id;
    final userDoc = FirebaseFirestore.instance.collection('users').doc(userId);

    final currentPasswordHash = await encryptPasswordCompute(currentPassword);
    final userSnapshot = await userDoc.get();
    final userData = userSnapshot.data();
    final currentPasswordFromDB = userData?['password'].toString();

    if (currentPasswordFromDB == currentPasswordHash) {
      final newPasswordHash = await encryptPasswordCompute(newPassword);

      UserService().updateUserPassword(userId, newPasswordHash).then((_) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Password Changed'),
              content: Text('The password has been changed successfully.'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.pushNamed(context, '/home');
                  },
                  child: Text('OK'),
                ),
              ],
            );
          },
        );
      }).catchError((error) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Error Changing Password'),
              content: Text('There was an error changing the password: $error'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('OK'),
                ),
              ],
            );
          },
        );
      });
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error Changing Password'),
            content: Text('The current password does not match.'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('OK'),
              ),
            ],
          );
        },
      );
    }
  }

  void changePhoneNumber() {
    final uvm = Provider.of<GlobalUserViewModel>(context, listen: false);
    final userId = uvm.id;

    UserService().updateUserPhone(userId, newPhoneNumber).then((value) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Phone Number Changed'),
            content: Text('The phone number has been changed successfully.'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.pushNamed(context, '/home');
                },
                child: Text('OK'),
              ),
            ],
          );
        },
      );
    }).catchError((error) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error Changing Phone Number'),
            content: Text('There was an error changing the phone number: $error'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('OK'),
              ),
            ],
          );
        },
      );
    });
  }

  void changeSemester() {
    final uvm = Provider.of<GlobalUserViewModel>(context, listen: false);
    final userId = uvm.id;

    UserService().updateUserSemester(userId, newSemester).then((value) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Semester Changed'),
            content: Text('The semester has been changed successfully.'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.pushNamed(context, '/home');
                },
                child: Text('OK'),
              ),
            ],
          );
        },
      );
    }).catchError((error) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error Changing Semester'),
            content: Text('There was an error changing the semester: $error'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('OK'),
              ),
            ],
          );
        },
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    final uvm = Provider.of<GlobalUserViewModel>(context);
    print({'Data', uvm.id});
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Change Password'),
            TextField(
              onChanged: (value) {
                setState(() {
                  currentPassword = value;
                });
              },
              obscureText: true,
              decoration: InputDecoration(
                hintText: 'Enter current password',
              ),
            ),
            TextField(
              onChanged: (value) {
                setState(() {
                  newPassword = value;
                });
              },
              obscureText: true,
              decoration: InputDecoration(
                hintText: 'Enter new password',
              ),
            ),
            SizedBox(height: 16.0),
            Text('Change Phone Number'),
            TextField(
              onChanged: (value) {
                setState(() {
                  newPhoneNumber = value;
                });
              },
              decoration: InputDecoration(
                hintText: currentPhoneNumber,
              ),
            ),
            SizedBox(height: 16.0),
            Text('Change Semester'),
            TextField(
              onChanged: (value) {
                setState(() {
                  newSemester = value;
                });
              },
              decoration: InputDecoration(
                hintText: currentSemester,
              ),
            ),
            SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: saveChanges,
              child: Text('Save Changes'),
            ),
          ],
        ),
      ),
    );
  }
}
