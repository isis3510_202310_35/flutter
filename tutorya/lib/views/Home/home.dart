import "package:flutter/material.dart";
import "package:tutorya/singletons/singletonLightSensor.dart";
import "package:tutorya/tutor_ya_icons_icons.dart";
import "package:tutorya/views/mentorship_list/mentorship_enrolledUser.dart";
import "package:tutorya/views/mentorship_list/mentorship_list.dart";
import "package:tutorya/views/profileScreen/profile_view.dart";
import "./home2.dart";
import 'package:flutter/services.dart';
import '../upcoming/upcoming.dart';
import '../tutor_list_view.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  PageController pageController = PageController();
  bool showAlert = true;
  final LightSensor _lightSensor= LightSensor.instance;

  void showAlertPopup(BuildContext context) {
    if (_lightSensor.isItDark()) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Low Light'),
            content: Text('Do you want to change the app to Dark Mode?'),
            actions: <Widget>[
              TextButton(
                child: Text('No'),
                onPressed: () {
                  _lightSensor.activateDarkMode=false;
                  _lightSensor.messageAlreadyShowed=true;
                  _lightSensor.stopListening();
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text('Yes'),
                onPressed: () {
                  _lightSensor.activateDarkMode = true;
                  _lightSensor.messageAlreadyShowed=true;
                  _lightSensor.stopListening();
                  Navigator.of(context).pop();
                  setState(() {

                  });
                },
              ),
            ],
          );
        },
      );
    }
  }

  void onTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    pageController.animateToPage(index,
        duration: Duration(milliseconds: 300), curve: Curves.easeIn);
  }

  void setAlertState() {
    setState(() {showAlert = false;});
  }

  void openUpcoming() {
    Navigator.push (
      context,
      MaterialPageRoute(builder: (context) => Upcoming()),
    );
  }

  void openTutors() {
    Navigator.push (
      context,
      MaterialPageRoute(builder: (context) => TutorListView()),
    );
  }

  void individualMentFunc(bool online) {
    Navigator.push (
      context,
      MaterialPageRoute(builder: (context) => MentorshipsEnrolledUser(online: online)),
    );
  }

  static const String _title = 'Home';

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration.zero,
            () => showAlertPopup(context));
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return WillPopScope(
      onWillPop: () async {
        // Aquí puedes poner la lógica que desees cuando el usuario presione el botón de retroceder
        // Por ejemplo, si quieres que salga de la aplicación, puedes poner Navigator.of(context).pop(true);

        // En este ejemplo, simplemente no permitimos que el usuario regrese a la pantalla anterior
        return false;
      },
      child: Scaffold(
        extendBody: true,
        body: PageView(
          controller: pageController,
          children: [
            HomeView(setAlert: setAlertState, showAlert: showAlert, openUpcoming: openUpcoming, openIndividual: individualMentFunc, openTutors: openTutors),
            MentorshipListPage(),
            Container(color: Colors.yellow),
            ProfileScreen()
          ],
        ),
        bottomNavigationBar: CreateBottombar(context)));
  }

  Container CreateBottombar(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
//        boxShadow: [
//          BoxShadow(
//            color: Colors.black38,
//            offset: const Offset(0, 10),
//            spreadRadius: 0,
//            blurRadius: 10
//          ),
//        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: _lightSensor.isActivatedDark()==true ? Colors.blueGrey:Color(0xFFfaf2f2),
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(
                  TutorYaIcons.home,
                  size: 50,
                ),
                label: 'Home'),
            BottomNavigationBarItem(
                icon: Icon(TutorYaIcons.mentorship, size: 50), label: 'Mentorships'),
            BottomNavigationBarItem(
                icon: Icon(TutorYaIcons.chat, size: 50), label: 'Chat'),
            BottomNavigationBarItem(
                icon: Icon(TutorYaIcons.config, size: 50), label: 'Settings'),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: _lightSensor.isActivatedDark()==true ? Colors.white:Colors.black,
          unselectedItemColor: _lightSensor.isActivatedDark()==true ? Colors.white60:Colors.black45,
          onTap: onTapped,
          elevation: 10,
        ),
      ),
    );
  }
}
