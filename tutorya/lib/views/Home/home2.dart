import 'package:cloud_firestore/cloud_firestore.dart';
import "package:flutter/material.dart";
import 'package:intl/intl.dart';
import 'package:tutorya/services/userservice.dart';
import 'package:tutorya/singletons/singletonLightSensor.dart';
import "package:tutorya/tutor_ya_icons_icons.dart";
import "package:tutorya/viewModels/globalUser_view_model.dart";
import 'package:tutorya/views/mentorship_list/mentorship_enrolledUser.dart';
import "package:tutorya/views/mentorship_list/mentorship_list.dart";
import "package:tutorya/views/SignIn/signIn.dart";
import "../../widgets/round_button_img.dart";
import "../../widgets/round_button_text_SHADOW.dart";
import 'dart:async';
import 'package:tutorya/viewModels/mentorship_list_view_model.dart';
import 'package:tutorya/viewModels/mentorship_view_model.dart';
import 'package:provider/provider.dart';
import 'package:tutorya/singletons/singletonConnectivity.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import '../../widgets/profile_pic.dart';

class HomeView extends StatefulWidget {
  final Function setAlert;
  final bool showAlert;
  final Function openUpcoming;
  final Function openIndividual;
  final Function openTutors;
  const HomeView(
      {Key? key,
      required this.setAlert,
      required this.showAlert,
      required this.openUpcoming,
      required this.openIndividual,
      required this.openTutors})
      : super(key: key);

  @override
  State<HomeView> createState() => _HomeSFState();
}

class _HomeSFState extends State<HomeView> {
  Map _source = {ConnectivityResult.none: false};
  final LightSensor _lightSensor= LightSensor.instance;
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;
  bool online=true;
  late DateTime endTime;

  void resFunc() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => RegistrationScreen()),
    );
  }

  VoidCallback showUpcoming(uvm) {
    return () {
      online ? 
        widget.openUpcoming() : 
        showDialog<String> (
          context: context,
          builder: (BuildContext context) => AlertDialog (
            title: const Text("No internet conection"),
            content: const Text('To see the upcoming mentorships, please make sure to have an internet cnnection. (^‿^)'),
            actions: <Widget>[
              TextButton(
                child: const Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                }
              )
            ]
          )
        )
      ;
    };
  }

  @override
  void initState() {
    super.initState();
    _networkConnectivity.initialise();
    listenToNetworkConnectivity();
    endTime = DateTime.now();
    Provider.of<GlobalUserViewModel>(context, listen: false).obtainMentorships(online);
    if(online==true){
      Provider.of<MentorshipListViewModel>(context, listen: false).fetchMentorshipsNow();
    }
    calculateDuration();
  }

  void calculateDuration() async {
    final uvm = Provider.of<GlobalUserViewModel>(context, listen: false);
    Duration duration = endTime.difference(uvm.startTime);
    final userDoc = FirebaseFirestore.instance.collection('users').doc(uvm.id);
    final userSnapshot = await userDoc.get();
    final data = userSnapshot.data();

    if (data != null && data['logInAverageTime'] != null) {
      UserService().updateUserAverageLogInTime(uvm.id, ((data['logInAverageTime'] + duration.inSeconds)/2).toString());
    } else {
      UserService().updateUserAverageLogInTime(uvm.id, duration.inSeconds.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<MentorshipListViewModel>(context);
    final uvm = Provider.of<GlobalUserViewModel>(context);
    if (vm.mentorships.isNotEmpty && widget.showAlert) {
      Future.delayed(Duration.zero,
          () => _ShowMentorSh(context, widget.setAlert, vm.mentorships));
    }
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
        color: _lightSensor.isActivatedDark()==true ? Colors.black : const Color(0xFFD1EDBF),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 20, 10, 20),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        ProfilePic (
                          img_url: uvm.photo,
                          size: 80,
                        ),
                        Padding(
                            padding: const EdgeInsets.all(10),
                            child: Text(
                              "Hello, ${uvm.name}",
                              style: TextStyle(color: _lightSensor.isActivatedDark()==true ? Colors.white : const Color(0xFF000000)),
                            )),
                        ]),
                    Icon(
                      TutorYaIcons.notifications,
                      color: _lightSensor.isActivatedDark()==true ? Colors.white:Colors.black,
                      size: 68,
                      ),
                    ]),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RoundButtonImg(
                        img_url: 'assets/images/bot-CTA.png',
                        func: () => {},
                        height: 150,
                      )
                    ]),
              ),
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                child: Container(
                  color: _lightSensor.isActivatedDark()==true ? Colors.grey:Color(0xFFFFFFFF),
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: Column(children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            RoundButtonTextShadow(
                              text: 'See Upcoming Mentorships',
                              func: showUpcoming(uvm),
                              color: _lightSensor.isActivatedDark()==true? 0xFFFFFFFF:0xFFFFE7AB,
                            ),
                          ],
                        ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            RoundButtonTextShadow(
                              text: 'Individual mentorships',
                              func: () => {widget.openIndividual(online)},
                              color: _lightSensor.isActivatedDark()==true? 0xFFFFFFFF:0xFFFFE7AB,
                            )
                          ]),
                    ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          RoundButtonTextShadow(
                            text: 'See the available tutors!',
                            color: _lightSensor.isActivatedDark()==true? 0xFFFFFFFF:0xFFFFE7AB,
                            func: () => {widget.openTutors()},
                          )
                        ]),
                  ),
                ]),
              ),
            )
          ],
        ),
      ),
    );
  }
//            children: <Widget> [
//              RoundButtonTextShadow (
//                text: 'Monitoria 1',
//                func: () {
//                  Navigator.of(context).pop();
//                  pFun(2);
//                },
//              ),

  void _ShowMentorSh(
      BuildContext context, Function pFun, List<MentorshipViewModel> ments) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: const Text('These mentorships may interest you'),
              content: Container(
                  width: double.maxFinite,
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: ments.length,
                      itemBuilder: (context, index) {
                        final m = ments[index];
                        return Text (
                          '${m.name} width ${m.tutorName} at ${DateFormat('H:mm').format(m.date)}'
                        );
                      })),
              actions: <Widget>[
                TextButton(
                    child: const Text('Ok'),
                    onPressed: () {
                      pFun();
                      Navigator.of(context).pop();
                    })
              ]);
        });
  }

  Future<void> listenToNetworkConnectivity() async {
    _networkConnectivity.myStream.listen((source) {
      _source = source;
      switch (_source.keys.toList()[0]) {
        case ConnectivityResult.mobile:
          _source.values.toList()[0] ? online = true : online = false;
          break;
        case ConnectivityResult.wifi:
          _source.values.toList()[0] ? online = true : online = false;
          break;
        case ConnectivityResult.none:
        default:
          online = false;
      }
    });
  }
}
