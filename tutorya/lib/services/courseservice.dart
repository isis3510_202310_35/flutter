import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tutorya/models/course.dart';

class CourseService {
  final CollectionReference _coursesCollection =
  FirebaseFirestore.instance.collection('courses');

  // Obtener los 3 cursos con más conteos, ordenados por el count
  Future<List<Course>> getTopCourses() async {
    QuerySnapshot querySnapshot = await _coursesCollection
        .orderBy('count', descending: true)
        .limit(3)
        .get();

    return querySnapshot.docs
        .map((doc) => Course.fromJson(doc.data() as Map<String, dynamic>))
        .toList();
  }

  // Actualizar el conteo de un curso dado su nombre
  Future<void> updateCourseCount(String name) async {
    QuerySnapshot querySnapshot = await _coursesCollection
        .where('name', isEqualTo: name)
        .get();
    List<Course> c=querySnapshot.docs
        .map((doc) => Course.fromJson(doc.data() as Map<String, dynamic>)).toList();
    int count=c.first.count+1;
    final docId = querySnapshot.docs.first.id;

    await _coursesCollection.doc(docId).update({'count': count});
  }
}