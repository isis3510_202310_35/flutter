import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:flutter/foundation.dart';

String encryptPassword(String password) {
  var bytes = utf8.encode(password); // Convert to UTF-8 format
  var hash = sha256.convert(bytes); // Encrypt
  return hash.toString(); // Convert to hexadecimal
}

Future<String> encryptPasswordCompute(String password) {
  return compute(encryptPassword, password);
}
