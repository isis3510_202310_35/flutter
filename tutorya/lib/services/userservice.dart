import 'dart:async';
import 'dart:isolate';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tutorya/models/course.dart';
import 'package:tutorya/models/mentorship.dart';
import 'package:tutorya/models/userApp.dart';
import 'dart:convert';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tutorya/singletons/singletonDataBase.dart';
import '../cache/generalCache.dart';

class UserService {

  // Definir la estructura de la tabla de clics
  final String tableClicks = 'clicks';
  final String columnId = 'id';
  final String columnClicks = 'clicks';
  double average =0.0;

  final CollectionReference _usersCollection =
      FirebaseFirestore.instance.collection('users');

  final CollectionReference _mentorshipsCollection =
  FirebaseFirestore.instance.collection('mentorships');

  RootIsolateToken rootIsolateToken = RootIsolateToken.instance!;

  // DarkMode
  final LocalDatabase _db= LocalDatabase.instance;

  Future<List<UserApp>> getAllUsers() async {
    QuerySnapshot querySnapshot = await _usersCollection.get();
    return querySnapshot.docs
        .map((doc) => UserApp.fromJson(doc.data() as Map<String, dynamic>))
        .toList();
  }

  Future<List<UserApp>> getAllTutors() async {
    if (GeneralCache.isFresh()) {
      print("Using tutor cache :)");
      return GeneralCache.getList().cast<UserApp>();
    }
    final snapshot = await _usersCollection.where('type', isEqualTo: 1).get();
    List<UserApp> tutors = [];
    for (final doc in snapshot.docs) {
      var data = doc.data() as Map<String, dynamic>;
      // Recuperar datos de la colección de students
      var mentorshipRefs = List<DocumentReference>.from(data['mentorships']);
      var mentorshipDocs =
          await Future.wait(mentorshipRefs.map((ref) => ref.get()));
      var mentorshipDataList = mentorshipDocs.map((doc) => doc.data()).toList();
      data['mentorships'] = mentorshipDataList;
      for(int i=0;i<mentorshipDataList.length;i++){
        var mentorship=mentorshipDataList[i] as Map<String, dynamic>;
        // Recuperar datos de la colección de tutor
        var tutorRef = mentorship['tutor'] as DocumentReference;
        var tutorDoc = await tutorRef.get();
        var tutorData = tutorDoc.data() as Map<String, dynamic>;
        mentorship['tutor'] = tutorData;
        mentorshipDataList[i]=mentorship;
      }
      tutors.add(UserApp.fromJsonNoCourses(data));
      GeneralCache.add(UserApp.fromJsonNoCourses(data));
    }
    return tutors;
  }

  Future<UserApp> getUserById(String id) async {
    DocumentSnapshot docSnapshot = await _usersCollection.doc(id).get();
    return UserApp.fromJson(docSnapshot.data()! as Map<String, dynamic>);
  }

  Future<List<Mentorship>> getUserMentorshipsId(String id) async {
    DocumentSnapshot docSnapshot = await await _usersCollection.doc(id).get();
    if (docSnapshot.exists) {
      var u = docSnapshot.data() as Map<String, dynamic>;
      var mentsDR = List<DocumentReference>.from(u['mentorships']);
      var mentsD = await Future.wait(mentsDR.map((ref) => ref.get()));
      var ments = mentsD.map((doc) => doc.data()).toList();
      u['mentorships'] = ments;
      for(int i = 0; i < ments.length; i++) {
        var m = ments[i] as Map<String, dynamic>;
        var tutorRef = m['tutor'] as DocumentReference;
        var tutorDoc = await tutorRef.get();
        var tutorData = tutorDoc.data() as Map<String, dynamic>;
        m['tutor'] = tutorData;
        ments[i]=m;
      }
      return UserApp.fromJsonNoCourses(u).mentorships;
    } else {
      throw ErrorDescription("User not found");
    }
  }

  Future<UserApp> getUserByEmail(String email) async {
    QuerySnapshot querySnapshot = await _usersCollection.where("email", isEqualTo: email).get();
    if (querySnapshot.docs.isNotEmpty) {
      var u = querySnapshot.docs.first.data() as Map<String, dynamic>;
      var mentsDR = List<DocumentReference>.from(u['mentorships']);
      var mentsD = await Future.wait(mentsDR.map((ref) => ref.get()));
      var ments = mentsD.map((doc) => doc.data()).toList();
      u['mentorships'] = ments;
      for(int i = 0; i < ments.length; i++) {
        var m = ments[i] as Map<String, dynamic>;
        var tutorRef = m['tutor'] as DocumentReference;
        var tutorDoc = await tutorRef.get();
        var tutorData = tutorDoc.data() as Map<String, dynamic>;
        m['tutor'] = tutorData;
        ments[i]=m;
      }
      return UserApp.fromJsonNoCourses(u);
    } else {
      throw ErrorDescription("User not found");
    }
  }

  Future<void> createUser(UserApp user) async {
    final docRef = _usersCollection.doc();
    final id = docRef.id;
    user.id = id;
    await docRef.set(user.toJson());
  }

  Future<void> updateUser(UserApp user) async {
    await _usersCollection.doc(user.id).update(user.toJson());
  }

  Future<void> updateUserPhone(String id, String newPhoneNumber) async {
    await _usersCollection.doc(id).update({'phone': newPhoneNumber});
  }

  Future<void> updateUserPassword(String id, String newPassword) async {
    await _usersCollection.doc(id).update({'password': newPassword});
  }

  Future<void> updateUserSemester(String id, String newSemester) async {
    await _usersCollection.doc(id).update({'semester': int.parse(newSemester)});
  }

  Future<void> updateUserAverageLogInTime(String id, String seconds) async {
    await _usersCollection.doc(id).update({'logInAverageTime': double.parse(seconds)});
  }

  Future<void> deleteUser(String id) async {
    await _usersCollection.doc(id).delete();
  }

  Future<List<UserApp>> getStudentsByCareerWithMentorship(String career) async {
    final snapshot = await _usersCollection
        .where('type', isEqualTo: 2)
        .where('career', isEqualTo: career)
        .where('numMentorships', isGreaterThan: 0)
        .get();

    final students = <UserApp>[];
    for (final doc in snapshot.docs) {
      var data = doc.data() as Map<String, dynamic>;

      // Recuperar datos de la colección de students
      var mentorshipRefs = List<DocumentReference>.from(data['mentorships']);
      var mentorshipDocs =
          await Future.wait(mentorshipRefs.map((ref) => ref.get()));
      var mentorshipDataList = mentorshipDocs.map((doc) => doc.data()).toList();
      data['mentorships'] = mentorshipDataList;
      for(int i=0;i<mentorshipDataList.length;i++){
        var mentorship=mentorshipDataList[i] as Map<String, dynamic>;
        // Recuperar datos de la colección de tutor
        var tutorRef = mentorship['tutor'] as DocumentReference;
        var tutorDoc = await tutorRef.get();
        var tutorData = tutorDoc.data() as Map<String, dynamic>;
        mentorship['tutor'] = tutorData;
        mentorshipDataList[i]=mentorship;
      }
      students.add(UserApp.fromJsonNoCourses(data as Map<String, dynamic>));
    }

    return students;
  }

  Future<void> updateUserPhoto(String userId, String photoUrl) async {
  await _usersCollection.doc(userId).update({'photo': photoUrl});
  }

  // Future<void> addMentorshipToUserIsolate(String userId, Mentorship mentorship) async {
  //  await compute(_addMentorshipToUserIsolate, {'userId': userId, 'mentorship': mentorship.toJson()});
  //}

  Future<void> addMentorshipToUser(String userId, Mentorship mentorship) async {
    // final String userId = params['userId'];
    // final Mentorship mentorship = Mentorship.fromJson(params['mentorship']);
    // Obtener la referencia del documento de la mentorship
    final DocumentReference mentorshipRef = _mentorshipsCollection.doc(mentorship.id);
    // Obtener el documento del usuario
    final DocumentSnapshot userSnapshot = await _usersCollection.doc(userId).get();
    print(userSnapshot);
    final userData = userSnapshot.data() as Map<String, dynamic>?;

    // Verificar si el documento existe y contiene el campo 'mentorships'
    if (userData != null && userData.containsKey('mentorships')) {
      final int numMent=userData['numMentorships'];
      final List<dynamic> mentorships = userData['mentorships'];

      // Agregar la nueva mentorship al arreglo existente
      mentorships.add(mentorshipRef);

      // Actualizar el documento del usuario con el nuevo arreglo de mentorships
      await _usersCollection.doc(userId).update({'mentorships': mentorships, 'numMentorships': numMent+1});
    }

    return;
  }

  // Método para guardar la mentorship en un archivo local privado
  Future<void> saveMentorshipToLocalFile(Mentorship mentorship) async {
    await compute(_saveMentorshipToLocalFileIsolate, mentorship.toJson());
  }

  void _saveMentorshipToLocalFileIsolate(dynamic mentorshipJson) async {
    BackgroundIsolateBinaryMessenger.ensureInitialized(rootIsolateToken);
    final file = await _getLocalFile();
    List<Mentorship> mentorships = [];

    if (await file.exists()) {
      final mentorshipsJson = await file.readAsString();
      final mentorshipsData = jsonDecode(mentorshipsJson) as List<dynamic>;
      mentorships = mentorshipsData.map((data) => Mentorship.fromJson(data)).toList();
    }

    if (mentorships.length >= 10) {
      mentorships.removeAt(0); // Eliminar la primera mentoría guardada
    }

    mentorships.add(Mentorship.fromJson(mentorshipJson));

    final mentorshipsJson = jsonEncode(mentorships.map((mentorship) => mentorship.toJson()).toList());
    await file.writeAsString(mentorshipsJson);
  }


  Future<File> _getLocalFile()  async {
    final directory = await getApplicationDocumentsDirectory();
    return File('${directory.path}/mentorship.json');
  }

// Método para cargar las mentorships desde el archivo local privado
  Future<List<Mentorship>> loadMentorshipsFromLocalFile() async {
    return await compute(_loadMentorshipsFromLocalFileIsolate, null);
  }

  Future<List<Mentorship>> _loadMentorshipsFromLocalFileIsolate(dynamic _) async {
    BackgroundIsolateBinaryMessenger.ensureInitialized(rootIsolateToken);
    final file = await _getLocalFile();
    if (file.existsSync()) {
      final mentorshipsJson = file.readAsStringSync();
      final mentorshipsData = jsonDecode(mentorshipsJson) as List<dynamic>;
      return mentorshipsData.map((data) => Mentorship.fromJson(data)).toList();
    } else {
      return [];
    }
  }

  // Actualizar el conteo de un curso dado su nombre
  Future<void> updateUserClicks(String id, int clicks) async {
    await _usersCollection.doc(id).update({'clicks': clicks});
  }

  // Método para cargar las mentorships desde el archivo local privado
  Future<void> calculateAverageClicks(String id) async {
    List<int> clicks=await getAllClicks();
    Map<String, dynamic> args = {'id': id, 'clicks': clicks};
    print(clicks);
    double average = await compute(_calculateAverageClicksIsolate, args);
    uploadAverageClicks(id, average);
  }

  // Insertar un nuevo clic en la base de datos
  Future<void> insertClick(int clicks) async {
    _db.insertClick(clicks);
  }

  // Obtener todos los clics de la base de datos
  Future<List<int>> getAllClicks() async {
    return await _db.getAllClicks();
  }

  // Función que se ejecuta dentro de compute
  Future<double> _calculateAverageClicksIsolate(Map<String, dynamic> args) async {
    String id = args['id'];
    List<int> clicks = args['clicks'];

    if (clicks.isEmpty) {
      return 0.0;
    }

    int totalClicks = clicks.reduce((a, b) => a + b);
    double average = totalClicks / clicks.length;
    print(average);
    return average;
  }

  // Calcular el promedio de los clics
  Future<void> uploadAverageClicks(String id, double average) async {
    print(average);
    _usersCollection.doc(id).update({'clicks': average});
  }

}
