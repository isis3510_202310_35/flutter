import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tutorya/models/mentorship.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MentorshipService {
  final CollectionReference _mentorshipsCollection =
  FirebaseFirestore.instance.collection('mentorships');
  final CollectionReference _usersCollection =
  FirebaseFirestore.instance.collection('users');
  int cantOfMentCache=0;


  Future<List<Mentorship>> getAllMentorships() async {
    QuerySnapshot querySnapshot = await _mentorshipsCollection.get();
    return querySnapshot.docs
        .map((doc) => Mentorship.fromJson(doc.data() as Map<String, dynamic>))
        .toList();
  }

  Future<Mentorship> getMentorshipById(String id) async {
    DocumentSnapshot docSnapshot = await _mentorshipsCollection.doc(id).get();
    var data = docSnapshot.data() as Map<String, dynamic>;
    // Recuperar datos de la colección de tutor
    var tutorRef = data['tutor'] as DocumentReference;
    var tutorDoc = await tutorRef.get();
    var tutorData = tutorDoc.data() as Map<String, dynamic>;
    data['tutor'] = tutorData;

    // Recuperar datos de la colección de students
    var studentRefs = List<DocumentReference>.from(data['students']);
    var studentDocs = await Future.wait(studentRefs.map((ref) => ref.get()));
    var studentDataList = studentDocs.map((doc) => doc.data()).toList();
    data['students'] = studentDataList;

    Mentorship ment=Mentorship.fromJson(data);

    //Save in UserPreferences
    if(cantOfMentCache<10){
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('mentorship_$id', jsonEncode(ment));
      cantOfMentCache++;
    }

    return ment;
  }

  Future<List<Mentorship>> getAllMentorshipsShared() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    final mentorshipKeys = _prefs.getKeys().where((key) => key.startsWith('mentorship_'));
    List<Mentorship> mentorships = [];

    for (final key in mentorshipKeys) {
      final mentorshipJson = _prefs.getString(key);
      final mentorship = Mentorship.fromJson(jsonDecode(mentorshipJson!));
      mentorships.add(mentorship);
    }

    return mentorships;
  }

  Future<void> clearSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    cantOfMentCache=0;
    await prefs.clear();
  }

  Future<void> createMentorship(Mentorship mentorship) async {
    await _mentorshipsCollection.add(mentorship.toJson());
  }

  Future<void> updateMentorship(Mentorship mentorship) async {
    await _mentorshipsCollection.doc(mentorship.id).update(mentorship.toJson());
  }

  Future<void> deleteMentorship(String id) async {
    await _mentorshipsCollection.doc(id).delete();
  }

  Future<List<Mentorship>> getAllMentorshipsByCourse(String course) async {
    QuerySnapshot querySnapshot = await _mentorshipsCollection
        .where('course', isEqualTo: course)
        .get();
    List<Mentorship> mentorships = [];
    for (var doc in querySnapshot.docs) {
      var data = doc.data() as Map<String, dynamic>;

      // Recuperar datos de la colección de tutor
      var tutorRef = data['tutor'] as DocumentReference;
      var tutorDoc = await tutorRef.get();
      var tutorData = tutorDoc.data() as Map<String, dynamic>;
      tutorData['id'] = tutorRef.id; // Agregar el ID del tutor al mapa de datos del tutor
      data['tutor'] = tutorData;

      // Recuperar datos de la colección de students
      var studentRefs = List<DocumentReference>.from(data['students']);
      var studentDocs = await Future.wait(studentRefs.map((ref) => ref.get()));
      var studentDataList = studentDocs.map((doc) => doc.data()).toList();
      data['students'] = studentDataList;

      mentorships.add(Mentorship.fromJson(data));
      //Save in UserPreferences
      if(cantOfMentCache<10){
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String id=Mentorship.fromJson(data).id;
        prefs.setString('mentorship_$id', jsonEncode(Mentorship.fromJson(data)));
        cantOfMentCache++;
      }
    }
    return mentorships;
  }

  Future<List<Mentorship>> getAllMentorshipsStartingSoon() async {
    QuerySnapshot querySnapshot = await _mentorshipsCollection.get();
    List<Mentorship> mentorships = [];
    for (var doc in querySnapshot.docs) {
      var data = doc.data() as Map<String, dynamic>;

      var now = DateTime.now();
      var mentDate = DateTime.parse(data['date']);

      if(now.hour == mentDate.hour-1 || now.hour == mentDate.hour-2 || now.hour == mentDate.hour-3) {

        // Recuperar datos de la colección de tutor
        var tutorRef = data['tutor'] as DocumentReference;
        var tutorDoc = await tutorRef.get();
        var tutorData = tutorDoc.data() as Map<String, dynamic>;
        tutorData['id'] = tutorRef.id; // Agregar el ID del tutor al mapa de datos del tutor
        data['tutor'] = tutorData;

        // Recuperar datos de la colección de students
        var studentRefs = List<DocumentReference>.from(data['students']);
        var studentDocs = await Future.wait(studentRefs.map((ref) => ref.get()));
        var studentDataList = studentDocs.map((doc) => doc.data()).toList();
        data['students'] = studentDataList;

        mentorships.add(Mentorship.fromJson(data));
      }
    }
    return mentorships;
  }

  Future<List<Mentorship>> getUpcomingMentorshipsFor(String name) async {
    QuerySnapshot querySnapshot = await _mentorshipsCollection.get();
    List<Mentorship> mentorships = [];
    for (var doc in querySnapshot.docs) {
      var data = doc.data() as Map<String, dynamic>;

      var now = DateTime.now();
      var three_late = now.add(const Duration(days:3));
      var mentDate = DateTime.parse(data['date']);

      if(mentDate.isAfter(now) && mentDate.isBefore(three_late)) {

        // Recuperar datos de la colección de students
        var studentRefs = List<DocumentReference>.from(data['students']);
        var studentDocs = await Future.wait(studentRefs.map((ref) => ref.get()));
        var studentDataList = studentDocs.map((doc) => doc.data()).toList();
        data['students'] = studentDataList;
        for(var s in studentDocs) {
          var cs = s.data() as Map<String, dynamic>;
          if(cs['name'] == name) {
            
            // Recuperar datos de la colección de tutor
            var tutorRef = data['tutor'] as DocumentReference;
            var tutorDoc = await tutorRef.get();
            var tutorData = tutorDoc.data() as Map<String, dynamic>;
            tutorData['id'] = tutorRef.id; // Agregar el ID del tutor al mapa de datos del tutor
            data['tutor'] = tutorData;

            mentorships.add(Mentorship.fromJson(data));
            break;
          }
        }
      }
    }
    return mentorships;
  }

  Future<void> addUserToMentorship(String userId, Mentorship mentorship) async {
    // final String userId = params['userId'];
    // final Mentorship mentorship = Mentorship.fromJson(params['mentorship']);
    // Obtener la referencia del documento de la mentorship
    final DocumentReference userRef = _usersCollection.doc(userId);
    // Obtener el documento del usuario
    final DocumentSnapshot mentSnapshot = await _mentorshipsCollection.doc(mentorship.id).get();
    print(mentSnapshot);
    final mentData = mentSnapshot.data() as Map<String, dynamic>?;

    // Verificar si el documento existe y contiene el campo 'mentorships'
    if (mentData != null && mentData.containsKey('students')) {
      final List<dynamic> students = mentData['students'];

      // Agregar la nueva mentorship al arreglo existente
      students.add(userRef);

      // Actualizar el documento del usuario con el nuevo arreglo de mentorships
      await _mentorshipsCollection.doc(mentorship.id).update({'students': students});
    }

    return;
  }
}
