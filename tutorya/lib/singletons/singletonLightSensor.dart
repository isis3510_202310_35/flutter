import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:light/light.dart';
class LightSensor {
  LightSensor._();
  static final _instance = LightSensor._();
  static LightSensor get instance => _instance;
  String _luxString = '100';
  late Light _light;
  late StreamSubscription _subscription;
  bool darkMode=false;
  bool messageAlreadyShowed=false;
  bool activateDarkMode=false;
  bool isDark=false;

  void onData(int luxValue) async {
    //print("Lux value: $luxValue");
    _luxString = "$luxValue";
  }

  String getLuxValue(){
    return _luxString;
  }

  void stopListening() {
    _subscription.cancel();
  }

  void startListening() {
    _light = new Light();
    try {
      _subscription = _light.lightSensorStream.listen(onData);
    } on LightException catch (exception) {
      print(exception);
    }
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    startListening();
  }

  bool isActivatedDark(){
    if(activateDarkMode==true){
      messageAlreadyShowed=true;
      darkMode=true;
    }
    else {
      activateDarkMode=false;
      darkMode=false;
    }
    return darkMode;
  }

  bool isItDark(){
    if(int.parse(getLuxValue())<100 && messageAlreadyShowed==false){
      isDark=true;
      messageAlreadyShowed=true;
    }
    else {
      isDark=false;
    }
    return isDark;
  }
}
