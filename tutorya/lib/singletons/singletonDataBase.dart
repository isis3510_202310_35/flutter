import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class LocalDatabase {
  LocalDatabase._();
  static final _instance = LocalDatabase._();
  static LocalDatabase get instance => _instance;

  static Database? _database;

  Future<Database> get database async {
    if (_database != null) {
      print("Ya fue creada");
      return _database!;
    }

    _database = await _initDatabase();
    print("Base de datos inicializada");
    return _database!;
  }

  // Insertar un nuevo clic en la base de datos
  Future<void> insertClick(int clicks) async {
    Database _database = await database;
    print(_database);
    await _database.insert(
      "clicks",
      {"clicks": clicks},
    );
  }

  // Obtener todos los clics de la base de datos
  Future<List<int>> getAllClicks() async {
    Database _database = await database;
    List<Map<String, dynamic>> results = await _database.query("clicks");
    List<int> clicks = results.map((row) => row["clicks"] as int).toList();
    return clicks;
  }

  Future<Database> _initDatabase() async {
    final appDirectory = await getDatabasesPath();
    final path = join(appDirectory, 'clicks.db');

    // Elimina la base de datos si ya existe
    // await deleteDatabase(path);

    return openDatabase(
      path,
      version: 1,
      onCreate: (db, version) async {
        await db.execute('''
          CREATE TABLE clicks (
            id TEXT PRIMARY KEY,
            clicks INTEGER
          )
        ''');
      },
    );
  }

  Future<void> closeDatabase() async {
    if (_database != null) {
      await _database!.close();
      _database = null;
    }
  }
}