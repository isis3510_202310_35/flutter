import "package:flutter/material.dart";

class RoundButtonImg extends StatelessWidget{

  final String img_url;
  final VoidCallback func;
  final double height;

  const RoundButtonImg({required this.img_url, required this.func, required this.height});
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: GestureDetector(
        onTap: func,
        child: Image.asset(
          img_url,
          height: height,
        ),
      ),
    );
  }
}
