import "package:flutter/material.dart";
import "package:cached_network_image/cached_network_image.dart";

class ProfilePic extends StatelessWidget {

  final String img_url;
  final double size;

  const ProfilePic({required this.img_url, required this.size});

  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: Colors.grey),
      ),
      child: ClipOval(
        child: CachedNetworkImage(
          placeholder: (context, url) => const CircularProgressIndicator(),
          imageUrl: img_url,
          fit: BoxFit.cover,
        ),
      )
    );
  }
}
