import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:tutorya/viewModels/globalUser_view_model.dart';
import 'package:tutorya/viewModels/mentorship_list_view_model.dart';
import 'package:tutorya/viewModels/mentorship_view_model.dart';
import 'package:tutorya/singletons/singletonLightSensor.dart';
import 'package:tutorya/views/mentorship_list/mentorship_detail.dart';

class MentorshipList extends StatelessWidget {
  final List<MentorshipViewModel> mentorships;
  final LightSensor _lightSensor= LightSensor.instance;

  MentorshipList({required this.mentorships});

  void openMentorshipDetail(context, mentorship) {
    Navigator.push (
      context,
      MaterialPageRoute(builder: (context) => MentorshipDetailDialog(mentorship: mentorship)),
    );
  }

  @override
  Widget build(BuildContext context) {
    final uvm = Provider.of<GlobalUserViewModel>(context);
    final vm = Provider.of<MentorshipListViewModel>(context);
    return ListView.builder(
      itemCount: mentorships.length,
      itemBuilder: (context, index) {
        final mentorship = mentorships[index];

        return InkWell(
          onTap: () {
            uvm.aumentarContadorClicks();
            vm.addCountCourse(mentorship.mentorship.course);
            openMentorshipDetail(context, mentorship);
          },
          child: Container(
            decoration: BoxDecoration(
              color: _lightSensor.isActivatedDark()==true ? Colors.grey:Colors.white,
              borderRadius: BorderRadius.circular(12),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 5,
                  offset: const Offset(0, 3),
                ),
              ],
            ),
            margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: ListTile(
              contentPadding: const EdgeInsets.all(10),
              leading: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(mentorship.photo),
                  ),
                  borderRadius: BorderRadius.circular(6),
                ),
                width: 50,
                height: 100,
              ),
              title: Text(
                mentorship.name,
                style: TextStyle(color: _lightSensor.isActivatedDark()==true ? Colors.white : const Color(0xFF000000)),
              ),
              subtitle: RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Tutor: ',
                      style: TextStyle(color: _lightSensor.isActivatedDark()==true ? Colors.white : const Color(0xFF000000)),
                    ),
                    TextSpan(
                      text: mentorship.tutorName,
                      style: TextStyle(color: _lightSensor.isActivatedDark()==true ? Colors.white : const Color(0xFF000000)),
                    ),
                  ],
                ),
              ),
              trailing: Text (
                DateFormat('MMM d (E) H:mm').format(mentorship.date),
                style: TextStyle(color: _lightSensor.isActivatedDark()==true ? Colors.white : const Color(0xFF000000)),
              )
            ),
          ),
        );
      },
    );
  }
}
