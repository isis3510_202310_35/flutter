import "package:flutter/material.dart";

class RoundButton extends StatelessWidget{

  final String text;
  final VoidCallback func;

  const RoundButton({required this.text, required this.func});
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: const Color(0xFFFFE7AB),
          minimumSize: Size(267, 42),
        ),
        onPressed: func,
        child: Text(
          text,
          style: TextStyle(color: Color(0xFF000000)),
        ),
      ),
    );
  }
}
