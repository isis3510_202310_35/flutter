import 'package:flutter/material.dart';
import '../viewModels/tutor_view_model.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class TutorList extends StatelessWidget {
  final List<TutorViewModel> tutors;
  TutorList({required this.tutors});

  @override
  Widget build(BuildContext context) {
    return ListView.builder (
      itemCount: this.tutors.length,
      itemBuilder: (context, index) {
        final tutor = this.tutors[index];

        return ListTile (
          contentPadding: EdgeInsets.all(10),
          leading: Container (
            decoration: BoxDecoration (
              image: DecorationImage (
                fit: BoxFit.cover,
                image: NetworkImage(tutor.getPhoto)
              ),
              borderRadius: BorderRadius.circular(6)
            ),
            width: 50,
            height: 100,
          ),
          title: Text(tutor.getName),
          trailing: RatingBarIndicator (
            rating: tutor.getRate.toDouble(),
            direction: Axis.horizontal,
            itemCount: 5,
            itemPadding: EdgeInsets.all(2),
            itemBuilder: (context, _) => Icon(
              Icons.star,
              color: Colors.amber,
              size: 10.0,
           ),
          ),
        );
      },
    );
  }
}
