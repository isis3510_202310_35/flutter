import "package:flutter/material.dart";

class RoundButtonTextShadow extends StatelessWidget{

  final String text;
  final VoidCallback func;
  final int color;

  const RoundButtonTextShadow({required this.text, required this.func, this.color = 0xFFFFFFFF});
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: Color(color),
          minimumSize: const Size(267, 42),
          elevation: 5,
        ),
        onPressed: func,
        child: Text(
          text,
          style: const TextStyle(color: const Color(0xFF000000)),
        ),
      ),
    );
  }
}
