class GeneralCache {
  static final List list = [];
  static DateTime lastUpdate = DateTime.now();

  static List getList() {
    return list;
  }

  static bool isFresh() {
    return DateTime.now().isBefore(lastUpdate.add(const Duration(minutes:10))) && list.isNotEmpty;
  }

  static bool add(dynamic element) {
    if(!list.contains(element)) {
      if(list.length < 50) {
        list.add(element);
      }
      else {
        list.removeAt(0);
        list.add(element);
      }
      lastUpdate = DateTime.now();
      return true;
    }
    else {
      lastUpdate = DateTime.now();
      return false;
    }
  }
}
