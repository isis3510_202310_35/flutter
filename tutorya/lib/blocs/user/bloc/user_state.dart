part of 'user_bloc.dart';

@immutable
abstract class UserState {
  final bool userIsLogged;
  final UserApp? user;

  const UserState({this.userIsLogged = false, this.user});
}

class UserInitialState extends UserState {
  const UserInitialState() : super(userIsLogged: false, user: null);
}

class UserSetState extends UserState {
  final UserApp userToSet;
  const UserSetState(this.userToSet)
      : super(userIsLogged: true, user: userToSet);
}
