part of 'user_bloc.dart';

@immutable
abstract class UserEvent {
}

class LogInUserEvent extends UserEvent {
  final UserApp user;
  LogInUserEvent(this.user);
}

class LogOutUserEvent extends UserEvent {
  final UserApp user;
  LogOutUserEvent(this.user);
}
