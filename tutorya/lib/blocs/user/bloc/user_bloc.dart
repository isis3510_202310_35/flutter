import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:tutorya/models/userApp.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(const UserInitialState()) {
    on<LogInUserEvent>((event, emit) {
      print('Log In User');
      emit(UserSetState(event.user));
    });
    on<LogOutUserEvent>((event, emit){
      print('Log Out User');
      emit(const UserInitialState());
    });
  }
}
