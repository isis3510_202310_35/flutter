import 'package:tutorya/models/userApp.dart';

class Mentorship {
  String id;
  String name;
  String course;
  bool available;
  String date;
  String description;
  int capacity;
  int duration;
  String type;
  UserApp tutor;
  List<UserApp> students;

  Mentorship({
    required this.id,
    required this.name,
    required this.course,
    required this.available,
    required this.date,
    required this.description,
    required this.capacity,
    required this.duration,
    required this.tutor,
    required this.type,
    this.students = const [],
  });

  factory Mentorship.fromJson(Map<String, dynamic> json) {
    return Mentorship(
      id: json['id'],
      name: json['name'],
      course: json['course'],
      available: json['available'],
      date: json['date'],
      description: json['description'],
      capacity: json['capacity'],
      duration: json['duration'],
      type: json['type'],
      tutor: UserApp.fromJsonNoRelations(json['tutor'] as Map<String, dynamic>),
      students: List<UserApp>.from(
          (json['students'] as List).map((user) => UserApp.fromJsonNoRelations(user))),
    );
  }

  factory Mentorship.fromJsonNoStudents(Map<String, dynamic> json) {
    return Mentorship(
      id: json['id'],
      name: json['name'],
      course: json['course'],
      available: json['available'],
      date: json['date'],
      description: json['description'],
      capacity: json['capacity'],
      duration: json['duration'],
      type: json['type'],
      tutor: UserApp.fromJsonNoRelations(json['tutor'] as Map<String, dynamic>),
      // students: List<UserApp>.from(
      //    (json['students'] as List).map((user) => UserApp.fromJsonIdNoRelations(user['data'],user['id']))),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'course': course,
      'available': available,
      'date': date,
      'description': description,
      'capacity': capacity,
      'duration': duration,
      'type': type,
      'tutor': tutor.toJson(),
      'students': List<dynamic>.from(students.map((user) => user.toJson())),
    };
  }
}
