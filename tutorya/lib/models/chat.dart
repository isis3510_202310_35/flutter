

import 'package:tutorya/models/message.dart';
import 'package:tutorya/models/userApp.dart';

class Chat {
  String id;
  List<UserApp> users;
  List<Message> messages;

  Chat({
    required this.id,
    required this.users,
    this.messages = const [],
  });

  factory Chat.fromJson(Map<String, dynamic> json) {
    return Chat(
      id: json['id'],
      users: List<UserApp>.from(
          (json['users'] as List).map((user) => UserApp.fromJsonNoRelations(user))),
      messages: List<Message>.from(
          (json['messages'] as List).map((message) => Message.fromJson(message))),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'users': List<dynamic>.from(users.map((user) => user.toJson())),
      'messages': List<dynamic>.from(messages.map((message) => message.toJson())),
    };
  }
}