import 'package:tutorya/models/userApp.dart';

class Message {
  String id;
  UserApp sender;
  String text;
  DateTime timestamp;

  Message({
    required this.id,
    required this.sender,
    required this.text,
    required this.timestamp,
  });

  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
      id: json['id'],
      sender: UserApp.fromJsonNoRelations(json['sender']),
      text: json['text'],
      timestamp: DateTime.parse(json['timestamp']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'sender': sender.toJson(),
      'text': text,
      'timestamp': timestamp.toIso8601String(),
    };
  }
}