import 'package:firebase_auth/firebase_auth.dart';
import 'package:tutorya/models/course.dart';
import 'package:tutorya/models/mentorship.dart';
import 'dart:convert';

class UserApp {
  String id;
  String name;
  String email;
  String phone;
  String photo;
  int type;
  int points;
  String password;
  String career;
  int semester;
  int rate;
  int numMentorships;
  List<Mentorship> mentorships;
  List<Course> courses;

  UserApp({
    required this.id,
    required this.name,
    required this.email,
    required this.phone,
    required this.photo,
    required this.type,
    required this.points,
    required this.password,
    required this.career,
    required this.semester,
    required this.rate,
    this.numMentorships = 0,
    this.courses =
        const [], // se inicializa la lista de cursos vacía por defecto
    this.mentorships = const [],
  });

  factory UserApp.fromJsonNoRelations(Map<String, dynamic> json) {
    return UserApp(
      id: json['id'],
      name: json['name'],
      email: json['email'],
      phone: json['phone'],
      photo: json['photo'],
      type: json['type'],
      points: json['points'],
      password: json['password'],
      career: json['career'],
      semester: json['semester'],
      rate: json['rate'],
      numMentorships: json['numMentorships'],
      // courses: List<Course>.from(
      //    (json['courses'] as List).map((course) => Course.fromJson(course))),
      // mentorships: List<Mentorship>.from(
      // (json['mentorships'] as List)
      //      .map((mentorship) => Mentorship.fromJson(mentorship)),
      // ),
    );
  }

  factory UserApp.fromJsonNoCourses(Map<String, dynamic> json) {
    return UserApp(
      id: json['id'],
      name: json['name'],
      email: json['email'],
      phone: json['phone'],
      photo: json['photo'],
      type: json['type'],
      points: json['points'],
      password: json['password'],
      career: json['career'],
      semester: json['semester'],
      rate: json['rate'],
      numMentorships: json['numMentorships'],
      // courses: List<Course>.from(
      //    (json['courses'] as List).map((course) => Course.fromJson(course))),
      mentorships: List<Mentorship>.from(
      (json['mentorships'] as List).map((mentorship) => Mentorship.fromJsonNoStudents(mentorship)),
      ),
    );
  }

  factory UserApp.fromJson(Map<String, dynamic> json) {
    return UserApp(
      id: json['id'],
      name: json['name'],
      email: json['email'],
      phone: json['phone'],
      photo: json['photo'],
      type: json['type'],
      points: json['points'],
      password: json['password'],
      career: json['career'],
      semester: json['semester'],
      rate: json['rate'],
      numMentorships: json['numMentorships'],
      courses: List<Course>.from(
          (json['courses'] as List).map((course) => Course.fromJson(course))),
      mentorships: List<Mentorship>.from(
      (json['mentorships'] as List)
      .map((mentorship) => Mentorship.fromJsonNoStudents(mentorship)),
      ),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'email': email,
      'phone': phone,
      'photo': photo,
      'type': type,
      'points': points,
      'password': password,
      'career': career,
      'semester': semester,
      'numMentorships': numMentorships,
      'courses': List<dynamic>.from(courses.map((course) => course.toJson())),
      'mentorships': List<dynamic>.from(mentorships.map((e) => e.toJson())),
    };
  }

  List<Mentorship> get userMentorships {
    if (type == 1) {
      return mentorships
          .where((mentorship) => mentorship.tutor == this)
          .toList();
    } else if (type == 2) {
      return mentorships
          .where((mentorship) => mentorship.students.contains(this))
          .toList();
    }
    return [];
  }

  copyWith({
    String? id,
    String? name,
    String? email,
    String? phone,
    String? photo,
    int? type,
    int? points,
    String? password,
    String? career,
    int? semester,
    int? numMentorships,
    List<Mentorship>? mentorships,
    List<Course>? courses,
  }) =>
      UserApp(
          id: id ?? this.id,
          name: name ?? this.name,
          email: email ?? this.email,
          phone: phone ?? this.phone,
          photo: photo ?? this.photo,
          type: type ?? this.type,
          points: points ?? this.points,
          password: password ?? this.password,
          career: career ?? this.career,
          semester: semester ?? this.semester,
          rate: rate ?? this.rate);
}
